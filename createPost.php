<html>
    <head>
        <link rel='stylesheet' href='createPost.css'>
        <link rel='stylesheet' href='management.css'>
        <link rel='stylesheet' href='modal.css'>
        <script src='modal.js' type='text/javascript'></script>
        <script src='changeLbl.js' type='text/javascript'></script>
        <title> Libertine Tattoo - Create Post</title>
    </head>
    <body>

        <div id='myModal' class='modal'>
            <div class='modal-content'>
                <div class='modal-header'>
                    <span type='button' class='close' onclick='closeModal()'>&times;</span>
                    <h2>Libertine Tattoo</h2>
                </div>
                <div class='modal-body'>
                    <p id='modal-text'>This is the text area</p>
                    <button type='button' class='modal-butt' id='noButt' onclick='cancelClick("manageWebsite.php")'>No</button>
                    <button type='button' class='modal-butt' id='yesButt' onclick='closeModal()'>Yes</button>
                </div>
            </div>
        </div>
        <?php
            /********************************************************************************************************************************************
             *                                              php written by: Michael Barfuss   Date: March 10, 2018                                      *
             *                                             HTML written by: Chris Toth        Date: March 13, 2018                                      *
             ********************************************************************************************************************************************/

            require ('checkValidUser.php'); // contains function to validate user (returns bool)
            require ('connect.php');        // Contains function to connect to database (returns mysqli connection object)
            require_once "sessionTimer.php";
            session_start();
            $reqAccArr = array("Artist","Manager","Pleb");  // array containing required staff positions to access this page

            sessionTimer();

            /******************************************************************
            * Code found on stack overflow to get proper date and time format *
            *******************************************************************/
            function getDatetimeNow()   // returns the date and time in the format SQL accepts
            {

                $tz_object = new DateTimeZone('America/Edmonton');
                //date_default_timezone_set('America/Edmonton'); // alternate method

                $datetime = new DateTime();
                $datetime->setTimezone($tz_object);
                return $datetime->format('Y\-m\-d\ h:i:s');
            }

            if(checkValidUser($_SESSION['accessLVL'], $reqAccArr, "createPost.php")) // make sure checkValidUser returns bool
            {

                $title = $content = "";
                if(isset($_POST['title']))
                {
                    $title = $_POST['title'];
                }

                if(isset($_POST['content']))
                {
                    $content = $_POST['content'];
                }

                $sID = $_SESSION['sID']; // ID retrieved at login
                //$sID = 7; // Testing purposes
                $portfolioID;
                $parent = '';
                $fieldsFilled = false;
                $conn = connect();

                //die($_POST['parent'] . "73");
                if(isset($_POST['parent']))
                {
                    if ($_POST['parent'] == "managePost.php")
                    {
                        $portfolioID = 0;
                    }
                    else
                    {
                        $sql = $conn->prepare("SELECT PO_ID FROM PORTFOLIO WHERE S_ID = ?;");
                        $sql->bind_param("i", $sID);
                        $sql->execute();
                        if ($sql->error)
                        {
                            die($sql->error . "87");
                        }
                        $sql->bind_result($recievedPO_ID);
                        $sql->fetch();
                        $sql->close();
                        $portfolioID = $recievedPO_ID;
                        //die($sID . " on line: 89");
                    }
                }
                else
                {
                    header("location: manageWebsite.php");  // weren't directed here by valid page
                    die("Parent not set");
                }

                if (isset($_POST['create']))
                {

                    // checks the page that called this one. If no parent, return to manageWebsite
                    
                        /*****************************************************************************/
                        // Assign 0 or the ID associated with the logged in staff member's portfolio //
                        /*****************************************************************************/
                        
                    

                    

            /****************    Evaluates whether required fields are filled in    *******************/
                                                                                                    ////
                    if(isset($_POST['content'], $_POST['title'], $_FILES['image'])                  ////
                    && !empty($_FILES['image']['tmp_name'] && $_FILES['image']['error'] == 0))      ////
                    {                                                                               ////
                        $fieldsFilled = true;                                                       ////
                    }                                                                               ////
                    else                                                                            ////
                    {                                                                               ////
                        $fieldsFilled = false;                                                      ////
                    }                                                                               ////
            /******************************************************************************************/

                    if ($fieldsFilled === true)
                    {	
                        // formats image for database
                        $image = file_get_contents($_FILES['image']['tmp_name']);
                        $extension = strtolower(substr($_FILES['image']['name'], strrpos($_FILES['image']['name'], '.') + 1));
                        $finfo = finfo_open(FILEINFO_MIME_TYPE);
                        $mime = $_FILES['image']['type'];
                        finfo_close($finfo);

                        $currTime = getDatetimeNow();

            /*****************    Makes sure file is an image of proper type    *****************/
                                                                                                //
                        $extArr = array('png', 'jpg','jpeg','gif');                             //
                        if(!in_array($extension, $extArr))                                      //
                        {                                                                       //
                            echo "<script>"                                                     //
                            ."alert('File Format Must Be png, jpeg/jpg, or gif');"              //
                            ."</script>";                                                       //
                            header("location: updatePost.php");                                 //
                            die();                                                              //
                        }                                                                       //
            /************************************************************************************/
                                                                    
                        $imageExists = FALSE; 
                        $images = $conn->query("SELECT * FROM IMAGE");

            /***********    Checks if the recieved image already exists in the database    **********/
                        while($row1 = mysqli_fetch_assoc($images))                                  //
                        {                                                                           //
                            if($row1['IMG_DATA'] == $image && $row1['IMG_MIME'] == $mime            //
                            && $row1['IMG_EXT'] == $extension)                                     //
                            {                                                                       //
                                $imageID = $row1['IMG_ID'];                                         //
                                $image = $row1['IMG_DATA'];                                         //
                                $imageExists = TRUE;                                                //
                                break;                                                              //
                            }                                                                       //
                        }                                                                           //
            /****************************************************************************************/
                                                                            
            /*********************************    Insert Post into the database    **************************************/
                                                                                                                        //
                        $zero = 0;                                                                                      //
                        $sql = $conn->prepare("INSERT INTO POST (S_ID, P_TITLE, P_DATE, P_CONTENT, PO_ID, P_DELETED) VALUES (?, ?, ?, ?, ?, ?);"); //
                        $sql->bind_param("isssii", $sID, $title, $currTime, $content, $portfolioID, $zero);             //
                        $result = $sql->execute();                                                                      //
                        if ($sql->error)
                        {
                            die($sql->error);
                        }
                        $sql->close();      
                        //die($result . "171");                                                                                               //
                                                                                                                        //
                        /*$result = $conn->query("INSERT INTO POST (S_ID, P_TITLE, P_DATE, P_CONTENT, PO_ID, P_DELETED)"// 
                        ."VALUES ('$sID', '$title', '$currTime', '$content', $portfolioID, 0);");*/                     //
                        if(!$result)                                                                                    //
                        {                                                                                               //
                            die("post INSERT Failed.");                                                                 //
                        }                                                                                               //
            /************************************************************************************************************/

                        $null = NULL;
                        $delPost = $conn->prepare("DELETE FROM POST WHERE S_ID = ? AND P_TITLE = ? AND P_DATE = ? AND P_CONTENT = ? AND PO_ID = ? AND P_DELETED = ?;");
                        $imgID_SQL = $conn->prepare("SELECT IMG_ID FROM IMAGE WHERE IMG_DATA = ? AND IMG_EXT = ?;");
                        $postID_SQL = $conn->prepare("SELECT P_ID FROM POST WHERE (S_ID = ? AND P_TITLE = ? AND P_DATE = ? AND P_CONTENT = ? AND PO_ID = ? AND P_DELETED = ?);");
                        $delImageSQL= $conn->prepare("DELETE FROM IMAGE WHERE IMG_DATA = ? AND IMG_EXT = ?;");
                        $postImageINSERT = $conn->prepare("INSERT INTO POST_IMAGE (P_ID, IMG_ID) VALUES (?, ?);");

            /***********************************    If image does not exist yet, insert it into the database    *************************************/
                                                                                                                                                    //
                        if($imageExists === FALSE)                                                                                                  //
                        {                                                                                                                           //
                            $imgINSERT = $conn->prepare("INSERT INTO IMAGE (IMG_DATA, IMG_MIME, IMG_EXT) VALUES (?, ?, ?);");                       //
                            $imgINSERT->bind_param("bss", $null, $mime, $extension);                                                                //
                            $imgINSERT->send_long_data(0, $image);                                                                                  //
                            $result = $imgINSERT->execute();                                                                                        //
                            $imgINSERT->close();                                                                                                    //
                                                                                                                                                    //
                            //$result = $conn->query("INSERT INTO IMAGE (IMG_DATA, IMG_MIME, IMG_EXT) VALUES ('$image', '$mime', '$extension');");  //
                                                                                                                                                    //
                            if(!$result)                                                                                                            //
                            {
                                //testing purposes                                                                                                 //
                                /*$delPost->bind_param("isssii", $sID, $title, $currTime, $content, $portfolioID, $zero);                             //
                                $delPost->execute();                                                                                                //
                                $delPost->close();                                                                                                  //
                                                                                                                                                    //
                                /*$conn->query("DELETE FROM POST WHERE S_ID = '$sID' AND P_TITLE = '$title' AND P_DATE = '$currTime'"               //
                                ."AND P_CONTENT = '$content' AND PO_ID = '$portfolioID' AND P_DELETED = 0;");*/                                     //
                                                                                                                                                    //
                                die("image INSERT Failed.");                                                                       //
                            }                                                                                                                       //
                            //$imageID = $conn->query("SELECT IMG_ID FROM IMAGE WHERE IMG_DATA = '$image' AND IMG_EXT = '$extension';");            //
                        }                                                                                                                           //
                                                                                                                                                    //
                        /*$postID = $conn->query("SELECT P_ID FROM POST WHERE (S_ID = '$sID' AND P_TITLE = '$title' AND P_DATE = '$currTime'"       //
                        ."AND P_CONTENT = '$content' AND PO_ID = '$portfolioID' AND P_DELETED = 0);");                                              */
            /****************************************************************************************************************************************/

            /**************************************    Get ID's to insert into POST_IMAGE    ********************************************/
                                                                                                                                        //
                        $imgID_SQL->bind_param("bs", $null, $extension);                                                                //
                        $imgID_SQL->send_long_data(0, $image);                                                                          //
                        $imgID_SQL->execute();                                                                                          //
                        $imgID_SQL->bind_result($imageID);                                                                              //
                        $imgID_SQL->fetch();                                                                                            //
                        $imgID_SQL->close();                                                                                            //
                                                                                                                                        //
                        if(!$imageID || $imageID == NULL)                                                                               //
                        {                                                                                                               //
                            $delPost->bind_param("isssii", $sID, $title, $currTime, $content, $portfolioID, $zero);                     //
                            $delPost->execute();                                                                                        //
                            $delPost->close();                                                                                          //
                                                                                                                                        //
                            $delImageSQL->bind_param("bs", $null, $extension);                                                          //
                            $delImageSQL->send_long_data(0, $image);                                                                    //
                            $delImageSQL->execute();                                                                                    //
                            $delImageSQL->close();                                                                                      //
                                                                                                                                        //
                            //$conn->query("DELETE FROM IMAGE WHERE IMG_DATA = '$image';");                                             //
                            die("imageID Retrieval Failed.  Undoing Changes.");                                                         //
                        }                                                                                                               //
                                                                                                                                        //
                        $postID_SQL->bind_param("isssii", $sID, $title, $currTime, $content, $portfolioID, $zero);                      //
                        $postID_SQL->execute();                                                                                         //
                        $postID_SQL->bind_result($postID);                                                                              //
                        $postID_SQL->fetch();                                                                                           //
                        $postID_SQL->close();                                                                                           //
                                                                                                                                        //
                        if(!$postID || $postID == NULL)                                                                                 //
                        {                                                                                                               //
                            $delPost->bind_param("isssii", $sID, $title, $currTime, $content, $portfolioID, $zero);                     //
                            $delPost->execute();                                                                                        //
                            $delPost->close();                                                                                          //
                                                                                                                                        //
                            $delImageSQL->bind_param("bs", $null, $extension);                                                          //
                            $delImageSQL->send_long_data(0, $image);                                                                    //
                            $delImageSQL->execute();                                                                                    //
                            $delImageSQL->close();                                                                                      //
                                                                                                                                        //
                            //$conn->query("DELETE FROM IMAGE WHERE IMG_DATA = '$image';");                                             //
                            die("imageID Retrieval Failed.  Undoing Changes.");                                                         //
                        }                                                                                                               //
                                                                                                                                        //
            /****************************************************************************************************************************/

            /**************************************    Insert ID's into POST_IMAGE table    *************************************/
                                                                                                                                //
                        $postImageINSERT->bind_param("ii", $postID, $imageID);                                                  //
                        $result = $postImageINSERT->execute();                                                                  //
                        $postImageINSERT->close();                                                                              //
                                                                                                                                //
                        //$result = $conn->query("INSERT INTO POST_IMAGE (P_ID, IMG_ID) VALUES ($postID, $imageID);");          //
                                                                                                                                //
                        if(!$result)                                                                                            //
                        {                                                                                                       //
                            $delPost->bind_param("isssii", $sID, $title, $currTime, $content, $portfolioID, $zero);             //
                            $delPost->execute();                                                                                //
                            $delPost->close();                                                                                  //
                                                                                                                                //
                            $delImageSQL->bind_param("bs", $null, $extension);                                                  //
                            $delImageSQL->send_long_data(0, $image);                                                            //
                            $delImageSQL->execute();                                                                            //
                            $delImageSQL->close();                                                                              //
                                                                                                                                //
                            //$conn->query("DELETE FROM IMAGE WHERE IMG_DATA = '$image';");                                     //
                            die("POST_IMAGE INSERT Failed. Undoing Changes.");                                                  //
                        }                                                                                                       //
            /********************************************************************************************************************/
                    
                    $delImageSQL->close();
                    $delPost->close();

                    // Notify user that upload was successful    
                    echo "
                        <script>
                            displayModal();
                            setText('Upload was successful! <p>Would you like to create another?</p>');
                        </script>";

                    }
                    else if($_FILES['image']['error'] == 1)
                    {
                        echo "<script>
                            alert('Error Code: {$_FILES['image']['error']}');" // Echo error: upload error code
                        ."</script>";
                    }
                    else if(empty($_FILES['image']['tmp_name']))
                    {
                        echo"
                        <script>
                            displayModal();
                            setText('You are forgetting to upload an image! <p>Would you like to try again?</p>');
                        </script>";
                    }
                }

                $conn->close(); // Close the connection to the database
                
                print "

                <nav>
                    <ul id='ulNav'>
                        <li id='liNav' style='float:left; background-color:#120fbf;'><a id='aNav' href = 'home.php' >Home</a></li>
                        <li id='liNav' style='float:left; background-color:#120fbf; border-right:none;'><a id='aNav' href = 'manageWebsite.php' >Main Menu</a></li>
                        <li id='liNav'><a id='aNav' href = 'managePortfolio.php' >My Portfolio</a></li>
                        <li id='liNav'><a id='aNav' href = 'managePosts.php' >Blog</a></li>
                        <li id='liNav'><a id='aNav' href = 'manageMerchandise.php' >Merchandise</a></li>
                        <li id='liNav'><a id='aNav' href = 'manageOrders.php' >Orders</a></li>
                        <li id='liNav' style='border-right:none;'><a id='aNav' href = 'manageEmployees.php' >Employees</a></li>
                        <li id='liNav' style='float:right; background-color:#120fbf;'><a id='aNav' href='logout.php'>Logout</a></li>
                    </ul>
                </nav>

                <div id='wrapper'>                         
                    <h1 align ='center'>Create New Post</h1>
                    <div align ='center' class='separate'>
                    <form action = '' method='post' enctype='multipart/form-data' class='formMargin'/>
                        <input type='text' value='{$_POST['parent']}' name='parent' hidden readonly />
                        <input type='text' value='".(isset($_POST['title']) ? $_POST['title'] : null)."' name='title' class='postTitle' placeholder='Title 50 character max' required />
                        <div style='margin-top:5px;margin-bottom:5px;'>
                            <textarea type='text' name='content' value='' maxlength='1000' class='postContent' placeholder='Description (1000 char MAX)' style='resize:none;' required >".(isset($_POST['content']) ? $_POST['content'] : null)."</textarea>
                        </div>
                        ". // concatenate strings to allow for these comments!
                        
                        // Used a stylized label tag to workaround input file type poor functionailty
                        // Adapted from https://tympanus.net/codrops/2015/09/15/styling-customizing-file-inputs-smart-way/
                        "
                        <input type='file' name='image' id='image' onchange='changeLbl()' />
                        <label for='image' id='fileUploader'>Choose a file...</label>


                        <input class ='button' type='submit' value='Save' name='create' formaction='createPost.php' />
                        <a href='manageWebsite.php'><input type='button' class ='delbutton' value='Back' /></a>
                    </form>
                </div>";
            }
            else
            {
                // User does not have access to this page. Redirect elsewhere
                header("location: invalid.php");
                die();
            }
        ?>
    </body>
</html>