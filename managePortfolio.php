<html>
    <head>
	    <script type="text/javascript">
/********************************************************************************************************************************************
 *                                              php written by: Michael Barfuss   Date: March 10, 2018                                      *
 *                                             HTML written by:_________________  Date: _______________                                     *
 ********************************************************************************************************************************************/	    
	    </script>

    </head>
    <body>
        <?php

            require_once ('checkValidUser.php');
            require_once ('connect.php');
            require_once "sessionTimer.php";
            session_start();

            $reqAccArr = array("Artist","Manager","Pleb");

            sessionTimer();

            if(checkValidUser($_SESSION['accessLVL'], $reqAccArr, '')) // checkValidUser returns bool
            {

                $sID = $_SESSION['sID']; // ID retrieved at login

                $conn = connect(); // Connect to database

                print "
                <!doctype html>
                    <head>
                        <script src='modal.js' type='text/javascript'></script>
                        <link rel='stylesheet' href='managepost.css'>
                        <link rel='stylesheet' href='management.css'>
                        <link rel='stylesheet' href='modal.css'>
                    </head>
                    <body>
                    
                        <div id='myModal' class='modal'>
                            <!-- Modal content -->
                            <div class='modal-content'>
                                <div class='modal-header'>
                                    <span type='button' class='close' onclick='return closeModal()'>&times;</span>
                                    <h2>Libertine Tattoo</h2>
                                </div>
                                <div class='modal-body'>
                                    <p id='modal-text'>Are you sure you want to delete this post?</p>
                                    <button type='button' class='modal-butt' id='noButt' onclick='closeModal()'>No</button>
                                    <button type='button' class='modal-butt' id='yesButt' onclick='confirmClickForm(\"deletePost.php?parent=managePortfolio.php\")'>Yes</button>
                                </div>
                            </div>
                        </div>
                

                        <nav>
                            <ul id='ulNav'>
                                <li id='liNav' style='float:left; background-color:#120fbf;'><a id='aNav' href = 'home.php' >Home</a></li>
                                <li id='liNav' style='float:left; background-color:#120fbf; border-right:none;'><a id='aNav' href = 'manageWebsite.php' >Main Menu</a></li>
                                <li id='liNav'><a id='aNav' href = 'managePortfolio.php' >My Portfolio</a></li>
                                <li id='liNav'><a id='aNav' href = 'managePosts.php' >Blog</a></li>
                                <li id='liNav'><a id='aNav' href = 'manageMerchandise.php' >Merchandise</a></li>
                                <li id='liNav'><a id='aNav' href = 'manageOrders.php' >Orders</a></li>
                                <li id='liNav' style='border-right:none;'><a id='aNav' href = 'manageEmployees.php' >Employees</a></li>
                                <li id='liNav' style='float:right; background-color:#120fbf;'><a id='aNav' href='logout.php'>Logout</a></li>
                            </ul>
                        </nav>

                        
                        <div id='wrapper'>                         
                            <h1 align ='center'>Manage Portfolio</h1>";

                            //$result = $conn->query("SELECT * FROM POST WHERE S_ID = '$sID' AND PO_ID > 0 AND P_DELETED = 0");
                $zero = 0;
                $sql = $conn->prepare("SELECT * FROM POST WHERE S_ID = ? AND PO_ID > ? AND P_DELETED = ?");
                $sql->bind_param("iii", $sID, $zero, $zero);
                $sql->execute();
                $result = $sql->get_result();
                $sql->fetch();
                $sql->close();

                //$result = $conn->query("SELECT * FROM POST WHERE S_ID = '$sID' AND PO_ID = 0 AND P_DELETED = 0");

                /*************************************************************
                 *           Populates page with non-deleted posts           *
                 *************************************************************/
                while ($row = mysqli_fetch_assoc($result))
                {
                    // <input type='text' value='".$row['P_CONTENT']."' name='content' readonly />
                    echo "
                    <div align='center' class='separate'>
                        <form action = '' method='post' enctype='multipart/form-data' class='formMargin'/>
                            <input type='text' value='{$row['P_ID']}' name='pID' hidden readonly />
                            <input type='text' value='managePortfolio.php' name='parent' hidden readonly />
                            <input type='text' value='".$row['P_TITLE']."' name='title' class='postTitle' placeholder='Title 50 character max' readonly />
                            <input type = 'text' name = 'firstVisit' value = 'true' hidden readonly />
                            <div style='margin-top:5px;margin-bottom:5px;'>
                                <textarea type='text' name='content' value='{$row['P_CONTENT']}' maxlength='1000' class='postContent' placeholder='Description (1000 char MAX)' style='resize:none;' readonly>{$row['P_CONTENT']}</textarea>
                            </div>
                            <input class='button' type='submit' value='Update' name='update' formaction='updatePost.php' />
                            <input type='number' value='t' id='valid' hidden required>
                            <input type='button' class ='delbutton' value='Delete' name='delete' onclick='return displayModalForm(this.form)' />
                        </form>
                    </div>
                    ";
                }
                $result->free();
                $conn->close();
                // floating create post button
                echo"
                <a href = 'https://api.instagram.com/oauth/authorize/?client_id=0528a7658c5042ec8b901ea7a8185058&redirect_uri=https://www.libertine.ca/authinsta.php&response_type=code'>Link My Instagram</a>
                    <form action = 'createPost.php' method='post' enctype='multipart/form-data' class='formMargin'/>
                        <input type='text' name='parent' value='managePortfolio.php'  hidden readonly />
                        <input type='submit' value='Create New Portfolio Post' id='createButt'>
                    </form>
                ";
                
                // Closes the wrapper div
                echo "</div>";
            }
            else
            {
                // User does not have access to this page. Redirect elsewhere
                header("location: invalid.php");
                die();
            }
        ?>
    </body>
</html>