<?php
    require('connect.php');
    $conn = connect(); // Connect to database

    require_once("printHTML.php");
    $sql = $conn->query("SELECT POST.P_TITLE, POST.P_CONTENT, POST_IMAGE.IMG_ID, IMAGE.IMG_DATA, IMAGE.IMG_MIME FROM POST INNER JOIN POST_IMAGE ON POST.P_ID = POST_IMAGE.P_ID INNER JOIN IMAGE ON POST_IMAGE.IMG_ID = IMAGE.IMG_ID WHERE P_DELETED < 1  AND PO_ID =0 ORDER BY POST.P_ID DESC LIMIT 2;");
    $rows = mysqli_fetch_assoc($sql);
// Print HTML home page
// Please only use single quotes inside of the echo string below.
echo "
    <!doctype html>

    <head>
        <script src='jquery.min.js'></script>
        <script src='loadingscreen.js'></script>
        <script src='instafeed.js'></script>
        <script src='slideshow.js'></script>
        <link rel='stylesheet' href='loader.css'>
        <link rel='stylesheet' href='style.css'>
        <title> Libertine Tattoo - Home</title>
        <link href='https://fonts.googleapis.com/css?family=Work+Sans' rel='stylesheet'>
       
    </head>

    <body>";

    printLoader();
    printNav();
       echo"
    
    <div id='splashPhoto' class='tile'>
            <img class='center-logo' src='images/logo.png' height='55%' width='35%'></img>
    </div>
    
        

        <div id='aboutUs' class='tile'>
            <div class ='tileTitleLeft'>About Us</div>
                <img class='rightImg' src='images/shop8.jpg'></img>
                <div id='paragraphAboutUs'>
                Est. 2012. Libertine is a fully custom tattoo shop specializing in all styles with over
                15 years of combined experience in tattooing. We also provide Tattoo Removal and
                Fading. 
                </div>
            
            </div>

   
        <div id='gallery' class='tile' >
            <div id='after'></div>
            <div class ='tileTitle-gallery'>Gallery</div>
            
            <div class=container>

                <div id='slideshow'>
                    <div><img src='images/img1.jpg' /></div>
                    <div class='hidden'><img src='images/img2.jpg' /></div>
                    <div class='hidden'><img src='images/img3.jpg' /></div>
                    <div class='hidden'><img src='images/img4.jpg' /></div>
                    <div class='hidden'><img src='images/img5.jpg' /></div>
                    <div class='hidden'><img src='images/img6.jpg' /></div>
                </div>

            </div>
        </div>
             
        </div>
        
        <div id='newestBlogs' class='tile'>
            <div id='after2'>
            </div>
            <div class = 'blogcontain'>
            <image class='leftImg' src = 'data:".$rows['IMG_MIME'].";base64,".base64_encode($rows['IMG_DATA'])."' />
                <div class='paragraphBlog1'>
                     <h1>{$rows['P_TITLE']}</h1>
                     <p>{$rows['P_CONTENT']}</p>
                 </div>
            ";
                // SQL query above grabs two most recent rows of the post table.
                // Grab the next most recent.
            
                $rows = mysqli_fetch_assoc($sql);

            echo"   
            </div>

            <div class = 'blogcontain'>
            <div class='paragraphBlog2'>
                <h1>{$rows['P_TITLE']}</h1>
                <p>{$rows['P_CONTENT']}</p>
            </div>
                <image class='rightImg2' src = 'data:".$rows['IMG_MIME'].";base64,".base64_encode($rows['IMG_DATA'])."' />
            </div>
        
    </div>      
        

    <div id = 'contain'>
    </div>

        
        <div id='googleMaps' class='tile'>
            
            <div id='basicInfo'>   
                <iframe
                    alt='Google Map'
                    width='100%'
                    height='100%'
                    frameborder='0' style='border:1px; border-color:black;'
                    src='https://www.google.com/maps/embed/v1/search?key=AIzaSyAEIPhpKqkO5P_gBxk69G89Pbi2X-A876A
                    &q=Libertine,Lethbridge+Alberta' allowfullscreen>
                </iframe>
            </div>
        </div>
        
        ";
        printFooter();
       echo"
        
   
        <button onclick='topFunction()' id='myBtn' title='Go to top'>Top</button>    
    
        <script>

            window.onscroll = function() {scrollFunction()};

            function scrollFunction() {
                if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                    document.getElementById('myBtn').style.display = 'block';
                } else {
                    document.getElementById('myBtn').style.display = 'none';
                }
            }

            function topFunction() {
                document.body.scrollTop = 0;
                document.documentElement.scrollTop = 0;
            }

        </script>
        <script type='text/javascript'>
            var feed = new Instafeed({
                get: 'user'
                userID: '31216049'
                accessToken: '31216049.0528a76.951df7f58d0242fcad450b5650973168'
            });
            feed.run();
        </script>

    </body>

    </html>
"; // End of PHP echo to print HTML

/*footer possible

 <div class = 'homepage'><a class = 'whitetext' href='home.php'>FAQ</a></div>
                <div class = 'homepage'><a class = 'whitetext' href='home.php'>Shop</a></div>
                <div class = 'homepage'><a class = 'whitetext' href='home.php'>Artist</a></div>
                <div class = 'homepage'><a class = 'whitetext' href='home.php'>Blog</a></div>
                <div class = 'homepage'><a class = 'whitetext' href='home.php'>Home</a></div>
                
                
            <img class='logo' src='images/logo.png' height='60%' width='15%'></img>
            */

?>