<?php
    require_once "connect.php";
    session_start();

    if(isset($_GET['q'], $_GET['itemID'], $_GET['size'], $_SESSION['cart']["{$_GET['itemID']}"]["{$_GET['size']}"]))
    {
        $conn = connect();

        $itemId = $_GET['itemID'];

        $quantityCheck = $conn->prepare("SELECT * FROM INVENTORY WHERE M_ID = ?");
        $quantityCheck->bind_param("i", $itemId);
        $quantityCheck->execute();
        $sizes = $quantityCheck->get_result();
        $quantityCheck->fetch();
        $quantityCheck->close();

        $checkNum = mysqli_fetch_assoc($sizes)["{$_GET['size']}"];
        if($_GET['q'] < $checkNum || $_GET['q'] == $checkNum)
        {
            $_SESSION['cart']["{$_GET['itemID']}"]["{$_GET['size']}"] = $_GET['q'];
        }
        else
        {
            $_SESSION['cart']["{$_GET['itemID']}"]["{$_GET['size']}"] = $checkNum;
        }

        $itemSQL = $conn->prepare("SELECT * FROM MERCH_ITEM WHERE M_ID = ?;");
        
        $subtotal = 0;
        $GST = 0;
        foreach(array_keys($_SESSION['cart']) as $merchID)
        {
            $itemSQL->bind_param("i", $merchID);
            $itemSQL->execute();
            $itemData = $itemSQL->get_result();
            $itemSQL->fetch();
                    
            while ($itemInfo = mysqli_fetch_assoc($itemData))
            {
                foreach(array_keys($_SESSION['cart'][$merchID]) as $size)
                {
                    $price = ($itemInfo['M_PRICE']  * $_SESSION['cart'][$merchID][$size]);
                    $subtotal += $price;
                    $GST += (($price / 100) * 5); // 5 represents 5%
                }
            }
        }
        $conn->close();
        $_SESSION['subtotal'] = $subtotal;
        $_SESSION['GST'] = $GST;

        echo $_SESSION['cart']["{$_GET['itemID']}"]["{$_GET['size']}"];
    }
?>