<?php
    /*************************************************************************************************
    * This function will be called by basically every script on the manage website side of the site. *
    *    Inputs: requiredAccessArr is an array of the accepted access level for the current page.    *
    *                   access is the access level of the current logged in user.                    *
    **************************************************************************************************/
    /*****************    Depricated    ********************
    function checkValidUser($access, $requiredAccessArr)
    {
        foreach($requiredAccessArr as $reqAcc)
        {
            if ($access === $reqAcc)
            {
                return true;
                break;
            }
        }
        // If access level is not found. redirect.
        header("location: invalid.php");
        die();
    }
    ********************************************************/

    function checkValidUser($access, $requiredAccessArr, $page)
    {
        foreach($requiredAccessArr as $reqAcc)
        {
            if ($access === $reqAcc)
            {
                return true;
                break;
            }
        }
        // If access level is not found && $page is the managewebsite.php redirect.
        if($page == 'manageWebsite.php')
        {
            // If access level is not found && $page is the managewebsite.php redirect.
            header("location: login.php");
            die();
        }else
        {
            header("location: invalid.php");
            die();
        }
    }
?>