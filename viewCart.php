<html>
    <head>
        <link rel='stylesheet' href='style.css'>
        <link rel='stylesheet' href='viewCart.css'>
        <script src='quantity.js'></script>
        <title>Libertine - View Cart</title>
        <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
    </head>

<?php
    require "connect.php";
    require_once './config.php';
    require "printHTML.php";
    session_start();

    printNav();

    if(isset($_SESSION['cart']))
    {

        $hasItem = false;
        $hasCart = false;
        foreach(array_keys($_SESSION['cart']) as $merchID)
        {
            foreach(array_keys($_SESSION['cart'][$merchID]) as $size)
            {
                if($_SESSION['cart'][$merchID][$size] <= 0)
                {
                    unset($_SESSION['cart'][$merchID][$size]);
                }
                else
                {
                    $hasItem = true;
                    $hasCart = true;
                }
            }

            if(!$hasItem)
            {
                unset($_SESSION['cart'][$merchID]);
            }
        }
        if(!$hasCart)
        {
            unset($_SESSION['cart']);
            header("location: store.php");
            die();
        }

        $conn = connect();
        $itemSQL = $conn->prepare("SELECT * FROM MERCH_ITEM WHERE M_ID = ?");
        $subtotal = 0;

        echo"
        <div id='wrapper'>
            <h1 id='page-header'>Libertine Store</h1>
            <h2 id='page-subheader'>Items in your cart</h2>
            ";

        echo "<table cellspacing='0' cellpadding='15'>
            <tr>
                <th class='cell-left'>NAME</th>
                <th>SIZE</th>
                <th>QUANTITY</th>
                <th>PRICE</th>
            </tr>";
        foreach(array_keys($_SESSION['cart']) as $merchID)
        {
            $itemSQL->bind_param("i", $merchID);
            $itemSQL->execute();
            $itemData = $itemSQL->get_result();
            $itemSQL->fetch();
                    
            while ($itemInfo = mysqli_fetch_assoc($itemData))
            {
                foreach(array_keys($_SESSION['cart'][$merchID]) as $size)
                {
                    $price = ((number_format(($itemInfo['M_PRICE'] / 100.0), 2)) * $_SESSION['cart'][$merchID][$size]);
                    $subtotal += $price;
                    echo "
                    <tr class='item-row'>
                        <td class='cell-left'>{$itemInfo['M_NAME']}</td>
                        <td class='cell-center'>{$size}</td>
                        <td class='cell-center'>
                            <button type='button' id='upButt' class='quantityButts' onclick='upClickAJAX(\"quantity".$merchID.$size."\", ".$merchID.", \"".$size."\");'><i class='fas fa-arrow-up'></i></button>
                            <input type='text' class='quantity' id='quantity".$merchID.$size."' name='quantity".$merchID.$size."' form='order' value='{$_SESSION['cart'][$merchID][$size]}' readonly/>
                            <button type='button' id='downButt' class='quantityButts' onclick='downClickAJAX(\"quantity".$merchID.$size."\", ".$merchID.", \"".$size."\");'><i class='fas fa-arrow-down'></i></button>
                        </td>
                        <td class='cell-center'>".('$ '.number_format($itemInfo['M_PRICE'] / 100, 2))."</td>
                    </tr>";

                }
            }
        }
        $GST = number_format($_SESSION['GST'] / 100, 2);
        $subtotalFormatted = number_format($subtotal, 2);
        $total = number_format(($_SESSION['GST'] / 100) + $subtotal, 2);

        $GST = "$ ".$GST;
        $subtotalFormatted = "$ ".$subtotalFormatted;
        $total = "<b>$ ".$total."</b>";
        echo"
        <tr>
            <td colspan='3' class='cell-right'><b>GST:</b></td>
            <td class='cell-center' id='gst'>".$GST."</td>
        </tr>
        <tr>
            <td colspan='3' class='cell-right'><b>SUBTOTAL:</b></td>
            <td class='cell-center' id='subtotal'>".$subtotalFormatted."</td>
        </tr>
        <tr style='text-decoration:underline;'>
            <td colspan='3' class='cell-right'><b>TOTAL:</b></td>
            <td class='cell-center' id='total'>".$total."</td>
        </tr>
        <tr>
            <td colspan='4' class='cell-right'>
                <a href='checkout.php' class='checkoutButt' id='customButton'><span><i class='fas fa-shopping-cart'></i> Checkout</span></a>
            </td>
        </tr>
        </table>
        </div>";
/////////////////////////  Here down is stripe checkout code  ////////////////////////////
        echo"

    <form id='tokenForm' action='validate.php' method='post'>
        <input type='hidden' name='stripeToken' id='tkn' />
        <input type='hidden' name='postalCode' id='postalCode' />
        <input type='hidden' name='country' id='country' />
        <input type='hidden' name='province' id='province' />
        <input type='hidden' name='line1' id='line1' />
        <input type='hidden' name='city' id='city' />
        <input type='hidden' name='countryCode' id='countryCode' />
        <input type='hidden' name='custName' id='custName' />
        <input type='hidden' name='email' id='email' />
    </form>

    <script src='https://checkout.stripe.com/checkout.js'></script>

  

    <script>
        var handler = StripeCheckout.configure(
        {
            key: '{$stripe['publishable_key']}',
            image: '/images/shopping_cart.png',
            locale: 'auto',
            token: function(token, args)
            {
                
                if(args.shipping_address_country == 'Canada')
                {
                    // You can access the token ID with `token.id`.
                    // Get the token ID to your server-side code for use.
                    //alert(token.id);
                    var form = document.forms['tokenForm'];
                    form.elements.namedItem('stripeToken').value = token.id;
                    form.elements.namedItem('postalCode').value = args.shipping_address_zip;
                    form.elements.namedItem('country').value = args.shipping_address_country;
                    form.elements.namedItem('province').value = args.shipping_address_state;
                    form.elements.namedItem('line1').value = args.shipping_address_line1;
                    form.elements.namedItem('city').value = args.shipping_address_city;
                    form.elements.namedItem('countryCode').value = args.shipping_address_country_code;
                    form.elements.namedItem('custName').value = args.shipping_name;
                    form.elements.namedItem('email').value = token.email;
                    form.submit();
                }
                else
                {
                    location.reload(true);
                }
            }
        });
        
        document.getElementById('customButton').addEventListener('click', function(e)
        {
            // Open Checkout with further options:
            handler.open(
            {
                name: 'Libertine Merch',
                description: 'Checkout & Purchase',
                currency: 'cad',
                zipCode: 'true',
                billingAddress: 'true',
                shippingAddress: 'true',
                amount: ". ($_SESSION['subtotal'] + $_SESSION['GST']) ."
            });
            e.preventDefault();
        });

        // Close Checkout on page navigation:
        window.addEventListener('popstate', function()
        {
            handler.close();
        });
    </script>";

        $itemSQL->close();
        $conn->close();
        die();
    }
    else
    {
        header("location: store.php");
        die();
    }
?>