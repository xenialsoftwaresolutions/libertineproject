<?php
    require('connect.php');
    require('printHTML.php');

    if(isset($_GET['name']))
    {

        $conn = connect();

        $empName = $_GET['name'];
        $EXISTS = FALSE;
        $nameCHKsql = $conn->query("SELECT S_NAME FROM STAFF");
        while($nameCHK = mysqli_fetch_assoc($nameCHKsql)['S_NAME'])
        {
            if($nameCHK === $empName)
            {
                $EXISTS = TRUE;
                break;
            }
        }
        $nameCHKsql->free();

        if($EXISTS === FALSE)
        {
            header("location: artists.php");
            die();
        }

        $sql = $conn->prepare("SELECT * FROM STAFF WHERE S_NAME = ? AND S_DELETED = 0");
        $sql->bind_param("s", $empName);
        $sql->execute();
        $result = $sql->get_result();
        //$result = $conn->query("SELECT * FROM STAFF WHERE S_ID = '".$_POST['id']."'");
        if($result->num_rows == 0)
        {
            header("location: artists.php");
            die();
        }
        $artist = mysqli_fetch_assoc($result);
        $result->free();
        $sql->close();

        $sID = $artist['S_ID'];
        $sql = $conn->prepare("SELECT * FROM PORTFOLIO WHERE S_ID = ?;");
        $sql->bind_param("i", $sID);
        $sql->execute();
        $result = $sql->get_result();
        //$conn->query("SELECT PO_DESC FROM PORTFOLIO WHERE S_ID = '{$_POST['id']}'");
        $portfolio = mysqli_fetch_assoc($result);
        $result->free();
        $sql->fetch();
        $sql->close();

        if(!($portfolio['PO_DESC'] > ''))
        {
            $portfolio['PO_DESC'] = '';
        }

    echo "
    <html>
        <head>
            <script src='jquery.min.js'></script>
            <title>".$artist['S_NAME']."'s Portfolio</title>
            <link rel='stylesheet' href='loader.css'>
            <script src='loadingscreen.js'></script>
            <script type = 'text/javascript' src = 'jquery.min.js'></script>
            <link rel ='stylesheet' href = 'style.css'>
            <link rel ='stylesheet' href ='portfolio.css'>
            <script type='portfolio.js'></script>  
            <meta name='viewport' content='width=device-width, initial-scale=1.0'>
        </head>
        
        <body>";
        
        printLoader();
        printNav();
        
        echo"
            <h1 style='padding-top:10px;'> ".$artist['S_NAME']."'s Portfolio </h1>
            <div id = 'artistInfo' class = 'tile'>
            
                <div id = 'artistPhoto'>
                <div class='imageholder'>  <image width='100%' height='100%'  src = 'data:".$artist['S_MIME'].";base64,".base64_encode($artist['S_IMAGE'])."' ></div>
                </div>
            


                <div id = 'artistBio'>
                    <h3>BIO</h3>
                    
                    
                    <p>".$portfolio['PO_DESC']."</p>
                    <p>".$artist['S_EMAIL']."</p>
                </div>
            </div>
                <br>
                
                
            <div id ='artistArt'>
                <h3> Art </h3>
                <div class='larger'>
                    <div class='pictureholder'>
                    <img id='img' src ='images/img7.jpg' height='100%' width='100%' ></img>
                    </div>
                 </div>

                 <div class='larger'>
                    <div class='pictureholder'>
                    <img src ='images/img8.jpg' height='100%' width='100%'></img>
                    </div>
                </div>

                <div class='larger'>
                    <div class='pictureholder'>
                    <img src ='images/img9.jpg' height='100%' width='100%' ></img>
                    </div>
                </div>
            
              ";
                    
                    $limit = 10;
                    $poID = $portfolio['PO_ID'];
                    $postIDSQL = $conn->prepare("SELECT P_ID FROM POST WHERE PO_ID = ? ORDER BY P_ID DESC LIMIT ?");
                    $postIDSQL->bind_param("ii", $poID, $limit);
                    $postIDSQL->execute();
                    $postIDset = $postIDSQL->get_result();
                    $postIDSQL->fetch();
                    $postIDSQL->close();

                    $imageSQL = $conn->prepare("SELECT * FROM IMAGE WHERE IMG_ID = ?");
                    while($postID = mysqli_fetch_assoc($postIDset)['P_ID'])
                    {

                        $artworkSQL = $conn->prepare("SELECT * FROM POST_IMAGE WHERE P_ID = ?");
                        $artworkSQL->bind_param("i", $postID);
                        $artworkSQL->execute();
                        $artwork = $artworkSQL->get_result();
                        $artworkSQL->fetch();
                        $artworkSQL->close();

                        

                        while($imageID = mysqli_fetch_assoc($artwork)['IMG_ID'])
                        {
                            $imageSQL->bind_param("i", $imageID);
                            $imageSQL->execute();
                            $imageData = mysqli_fetch_assoc($imageSQL->get_result());
                            $imageSQL->fetch();

                            echo"
                          <div class='larger'>
                            <div class='pictureholder'>
                              
                                <image width='100%' height='100%'  src = 'data:".$imageData['IMG_MIME'].";base64,".base64_encode($imageData['IMG_DATA'])."' >
                                
                                
                            </div>
                            </div>
                            ";
                        }
                    }echo"</div>";
                    $imageSQL->close();

                    echo"
                    <h3>Posts</h3>
                    
                    <div class ='portpost'>
                                <div class='post'>
                                    <div class='posting'>
                                         <img src = 'images/boat.jpg' height='100%' width='100%' ></img>
                                    </div>
                                    <div class='posttext'>
                                         <h4> Title</h4> 
                                            
                                                <p> This is random text in a sentence, This is random text in a sentence,
                                                This is random text in a sentence, This is random text in a sentence,
                                                This is random text in a sentence, This is random text in a sentence,
                                                This is random text in a sentence, This is random text in a sentence,
                                                </p>
                                             
                                    </div>
                                </div> 
    
    
                           <div class='post1'>
                                <div class='posting'>
                                     <img src ='images/boat.jpg' height='100%' width='100%' ></img>
                                </div>
                                <div class='posttext'>
                                     <h4> Title</h4> 
                                        
                                            <p> This is random text in a sentence, This is random text in a sentence,
                                            This is random text in a sentence, This is random text in a sentence,
                                            This is random text in a sentence, This is random text in a sentence,
                                            This is random text in a sentence, This is random text in a sentence,
                                            </p>
                                         
                                </div>
                            </div> 
                                
                            
                    </div>
                    
                   
            ";
            printFooter();
        echo"
        </body>

        </html>";

        /* // Get the <span> element that closes the modal  // When the user clicks on <span> (x), close the modal
        Adapted from https://www.w3schools.com/howto/tryit.asp?filename=tryhow_css_modal_img
        <div id='popUp' class='pop'>
                        <span class='close'>&times;</span>
                        <img class='popUp-content' id='img1'>
                        
                    </div>

                    <script>

                            var modal = document.getElementById('myModal');

                        
                            var img = document.getElementById('img');
                            var popimg = document.getElementById('img1');
                        
                            img.onclick = function(){
                                pop.style.display = 'block';
                                popimg.src = this.src;
                                
                            }

                            
                            var span = document.getElementsByClassName('close')[0];

                            
                            span.onclick = function() { 
                                modal.style.display = 'none';
                            }
                </script>
                http://wickham43.net/hoverpopups.php

                  <div class='larger'>
                    <div class='pictureholder'>
                    <img id='img' src ='images/logo.png' height='100%' width='100%' ></img>
                    </div>
                 </div>

                 <div class='larger'>
                    <div class='pictureholder'>
                    <img src ='images/meme.jpg' height='100%' width='100%'></img>
                    </div>
                </div>

                <div class='larger'>
                    <div class='pictureholder'>
                    <img src ='images/boat.jpg' height='100%' width='100%' ></img>
                    </div>
                </div>



                 <div class='post'>
                                    <div class='posting'>
                                         <img src ='".$IMG_ID."' height='100%' width='100%' ></img>
                                    </div>
                                    <div class='posttext'>
                                         <h4> '".$P_TITLE."'</h4> <hr> 
                                            
                                                <p> '".$P_CONTENT."'
                                                </p>
                                             
                                    </div>
                                </div> 
        */
    }
    else
    {
        header("location: artists.php");
        die();
    }
?>