<?php
    // Chris Toth - March 10, 2018


    session_start();
    require_once "sessionTimer.php";
    require_once('connect.php');
    require_once('checkValidUser.php');

    $accessArray = array("Artist", "Manager");

    sessionTimer();

    if (checkValidUser($_SESSION['accessLVL'], $accessArray, 'manageWebsite.php'))
    {
        echo "
            <!doctype html>

            <head>
                <script src='jquery.min.js'></script>
                <link rel='stylesheet' href='management.css'>
                <link rel='stylesheet' href='manageWebsite.css'>
                <title> Libertine Tattoo - Manage Website</title>
            </head>
            <body>
                <nav>
                    <ul id='ulNav'>
                        <li id='liNav' style='float:left; background-color:#120fbf;'><a id='aNav' href = 'home.php' >Home</a></li>
                        <li id='liNav' style='float:left; background-color:#120fbf; border-right:none;'><a id='aNav' href = 'manageWebsite.php' >Main Menu</a></li>
                        <li id='liNav'><a id='aNav' href = 'managePortfolio.php' >My Portfolio</a></li>
                        <li id='liNav'><a id='aNav' href = 'managePosts.php' >Blog</a></li>
                        <li id='liNav'><a id='aNav' href = 'manageMerchandise.php' >Merchandise</a></li>
                        <li id='liNav'><a id='aNav' href = 'manageOrders.php' >Orders</a></li>
                        <li id='liNav' style='border-right:none;'><a id='aNav' href = 'manageEmployees.php' >Employees</a></li>
                        <li id='liNav' style='float:right; background-color:#120fbf;'><a id='aNav' href='logout.php'>Logout</a></li>
                    </ul>
                </nav>

                <div id='wrapper' style='height:650px;width:700px;'>
                    <h1 style='color:white;text-align:center;'>Welcome {$_SESSION['S_NAME']}</h1>
                    <a href='managePortfolio.php'><button class='manageButt'>My Portfolio</button></a>
                    <a href='managePosts.php'><button class='manageButt'>Blog</button></a>
                    <a href='manageMerchandise.php'><button class='manageButt'>Merchandise</button></a>
                    <a href='manageOrders.php'><button class='manageButt'>Orders</button></a>
                    <a href='manageEmployees.php'><button class='manageButt'>Employees</button></a>
                </div>

            </body>  
            </html>   
        ";
    }
    else
    {
        header("location: login.php");
        die();
    }
?>