Git workflow
****CHANGES****
Make sure that you are creating a new branch off of master.
**When you begin a day, or you know a pull request has been merged into master recently, make sure to go to bitbucket and sync your branch. This will make sure the branch you are working on is up to date with everything on origin/master. Then of course do a git pull so that your updates will be on your local branch.**

---


1. Pull **on Master branch**: 
Pull from the repository by using 'git pull'. This should be the first thing you do so that your local repository is updated with all the changes on the remote. Otherwise you may make changes that don't line up with changes someone else has made, and this could make you have to start over. 

2. Switch branches:
Switch to a branch other than master to make your changes by using 'git checkout [branch_name]'. DO NOT make changes on the master branch becuase you won't be able to push them. If a branch already exists for the work you are doing, switch to that branch. Otherwise create a branch by using 'git branch [branch_name]' and then checkout that branch. 
  

3. Make Changes:
 Make the  changes you want to make to your files. Add things, change stuff, do whatever you need to work on your stuff.

4. SAVE! 
Make sure that everything you did is saved. If you don't save it will cause weird conflicts when you try to pull.

5. Add:
Add the files you changed to the staging area by using 'git add [file_name file_name...]'. You could also use 'git add *' to just add all files. Then do a 'git status' to see what files are in the staging area before you commit them. Just double check you will be committing the correct files.

6. Commit:
Commit your changes using 'git commit'. This will bring up the commit message page where you type out the commit message. Here you put the title at the top. Two lines below this is the description. See 'Commit messages' below for some commit message requirements.

7. Pull:
Pull from the remote repository again to make sure you are up to date. This will probabaly bring up merge conflicts. See Merge Conflicts below. 

8. Commit the merge:
Commit the merge that just happened with the pull by using 'git commit'. For this you do not need to make a commit message because it should already have a commit message title saying that it is committing the merge from the remote to your local branch.

9. Push:
Push to the remote repository with the 'git push' command. This pushes all your changes up to the remote so that everyone can see them! If you are on a new branch you will have to set the upstream for this branch. Git will tell you that and give you the exact command you will need (which is 'git push --set-upstream origin [branch_name]').


Commit Messages:
The title of the commit message should be short but tell what files you edited and maybe the jist of what you edited. ie 'Added contact section to homepage' or 'Fixed Button logic on create post submit'.
The description should include more details of what you did and why. If something was broken and you fixed it include in point form what was wrong and what you did to fix it. 
Merge Conflicts:
VS Code handles these really well so look at the conflict message that git displays to see which files have merge conflicts, then look at those files in VS Code. VS Code will have some blocks of code highlighted and showing 'HEAD' or 'Incoming'. The HEAD changes are the changes you have made, and the 'Incoming' are the changes that someone else has made. At the top you have the options to accept HEAD changes, accept incoming changes, or accept both. Most of the time we will accept both, as long as accepting the incoming changes does'nt break your local code.

Adding Aliases:
//Our current method of setting aliases by editing the git config file allows only aliases for the first command, like 'g' or 'gs' or something. This will allow us to add other aliases so we can use 'g' and another alias like 'co' for checkout
git config --global alias.[alias] [command]
ie. Setting an alias for 'checkout' would look like this:
git config --global alias.co checkout

Git Command Cheat Sheet

git rebase -> Rebases off of master 
git pull -> this pulls everything from the remote repository and updates your local repository
git status -> shows you the difference between the working directory and the staging area
git add -> adds files to the staging area
git commit -> commits files in the staging area into the repository
git checkout [branch name]-> change the branch you are working on

Rebasing: 
We won't be rebasing anymore. We will be syncing instead. But this is here just in case anyone wants it. 
  When you checkout the branch you will be working on, you should make sure that it is still up to date with the master, because we will be pulling new stuff into master often. To make sure you are up to date with the new stuff, use 'git rebase origin/master' to rebase your current branch off of the remote master branch.












**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).