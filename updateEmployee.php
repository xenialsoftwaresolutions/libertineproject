<html>
        <head>
            <link rel='stylesheet' href='management.css'>
            <link rel='stylesheet' href='modal.css'>
            <script src='changeLbl.js'></script>
            <script src='modal.js'></script>
            <title>Libertine Tattoo - Update Employee</title>
        </head>
        <body>

        <div id='myModal' class='modal'>
            <div class='modal-content'>
                <div class='modal-header'>
                    <span type='button' class='close' onclick='closeModal()'>&times;</span>
                    <h2>Libertine Tattoo</h2>
                </div>
                <div class='modal-body'>
                    <p id='modal-text'>Are you sure you want to update with these values?</p>
                    <button type='button' class='modal-butt' id='noButt' onclick='closeModal()'>No</button>
                    <button type='button' class='modal-butt' id='yesButt' onclick='confirmClickForm("updateEmployee.php")'>Yes</button>
                </div>
            </div>
        </div>
    <?php
/********************************************************************************************************************************************
 *                                              php written by: Michael Barfuss   Date: March 10, 2018                                      *
 *                                             HTML written by: Chris Toth        Date: March 21, 2018                                      *
 ********************************************************************************************************************************************/

        require_once ('checkValidUser.php'); // contains function to validate user (returns bool)
        require_once ('connect.php');        // Contains function to connect to database (returns mysqli connection object)
        require ('salt.php');
        require_once "sessionTimer.php";
        session_start();
        $reqAccArr = array("Artist","Manager","Pleb");  // array containing required staff positions to access this page

        sessionTimer();

        if(checkValidUser($_SESSION['accessLVL'], $reqAccArr, '')) // make sure user is authorized
        {

            // if no Post ID, return to managePosts page
            if(!isset($_POST['sID']))
            {
                header("location: manageEmployees.php");
                die();
            }else
            {
                $empID = $_POST['sID'];
            }

            /******************    initialize name and content variables for post    **********************/
            $name = $content = "";                                                              /////////////
                                                                                                /////////////
                                                                                                /////////////
             /**********************************************************************************************/

            $sID = $_SESSION['sID']; // Staff member ID retrieved at login
            $username;
            $pword;

			
			$conn = connect();  // connect to database

            // checks the page that called this one. If no parent, return to manageWebsite
            if(isset($_POST['parent']))
            {
                $sql = $conn->prepare("SELECT S_NAME, S_UNAME, S_PWORD, S_EMAIL, S_PHONE, S_POSITION FROM STAFF WHERE S_ID = ?;");
                $sql->bind_param("i", $empID);
                $sql->execute();
                $result = $sql->get_result();
                $sql->fetch();
                $sql->close();

                $data = mysqli_fetch_assoc($result);
                
                $u_name = $data['S_UNAME']; // to populate the username field in the form
                $pword = $data['S_PWORD'];
                $email = $data['S_EMAIL'];
                $phone = $data['S_PHONE'];
                $position = $data['S_POSITION'];
                
                $result->free();
                if(isset($_POST['name']))
                {
                    $emp_name = $_POST['name'];   
                }
                else
                {
                    $emp_name = $data['S_NAME'];
                }

                if(isset($_POST['username']))
                {
                    $username = $_POST['username'];
                }
                else
                {
                    $username = $data['S_UNAME'];
                }

                $sql = $conn->prepare("SELECT PO_DESC FROM PORTFOLIO WHERE S_ID = ?;");
                $sql->bind_param("i", $empID);
                $sql->execute();
                $sql->bind_result($result2);
                $sql->fetch();
                $sql->close();
                // $result2 holds the description/bio selected from the portfolio table
                $bio = $result2;

            }else
            {
                header("location: manageWebsite.php");  // weren't directed here by valid page
                die();
            }

            if(isset($_POST['bio']))
            {
                $bio = $_POST['bio'];
            }
            
            /*************    Evaluates whether required fields are filled in    **************/
            //                                                                              ////
            if(isset($_POST['username'], $_POST['pword'], $_POST['email'], $_POST['phone'], ////
             $_POST['position']))                                                           ////
            {                                                                               ////
                $fieldsFilled = true;                                                       ////
                $pword = $_POST['pword'];
                    //$salt = "1234eryu80okjhgt567890pl";
                    //$saltedPword = $pword.$salt;
                    //$hashedPword = hash("sha256", $saltedPword);
                    //$pword = $hashedPword;                                                ////
                $email = $_POST['email'];                                                   ////
                $phone = $_POST['phone'];                                                   ////
                $position = $_POST['position'];                                             ////
            }else                                                                           ////
            {                                                                               ////
                $fieldsFilled = false;                                                      ////
            }                                                                               ////
            /**********************************************************************************/
            
            // check filled fields, error message not all fields filled

                // if fields filled, update post
	            if (!empty($fieldsFilled) && $fieldsFilled == true)
                {	
/***************************************************    Update STAFF Table    *******************************************************/
                    $sql = $conn->prepare("UPDATE STAFF SET S_UNAME = ?, S_PWORD = ?, S_POSITION = ?,"                              //
                    ."S_EMAIL = ?, S_PHONE = ?, S_SALT = ? WHERE S_ID = ?;");      

                    // hash the updated password
                    $salt = generateSalt();
                    $pword = $pword . $salt;        
                    $pword = hash("sha256", $pword, false);
                    
                    $sql->bind_param("ssssssi", $username, $pword, $position, $email, $phone, $salt, $empID);                       //
                    $result = $sql->execute();                                                                                      //
                    if(!$result)                                                                                                    //
                    {                                                                                                               //
                        die("<script>alert('employee UPDATE Failed.')</script>");                                                   //
                    }                                                                                                               //
                    $sql->close();                                                                                                  //
                                                                                                                                    //
                    /*$result = $conn->query("UPDATE STAFF SET S_UNAME = '".$_POST['uname']."', S_PWORD = '".$_POST['pword']."',"   //
                    ." S_POSITION = '".$_POST['position']."', S_EMAIL = '".$_POST['email']."',"                                     //
                    ."S_PHONE = '".$_POST['phone']."' WHERE S_ID = '$empID'"); */                                                   //
                                                                                                                                    //

                    if(isset($_FILES['image']) &&  !($_FILES['image']['error'] > UPLOAD_ERR_OK))
                    {
                        // formats image for database
                        $image = file_get_contents($_FILES['image']['tmp_name']);
                        //$image = mysqli_real_escape_string($conn, $image);
                        $extension = strtolower(substr($_FILES['image']['name'], strrpos($_FILES['image']['name'], '.') + 1));
                        $finfo = finfo_open(FILEINFO_MIME_TYPE);
                        $mime = $_FILES['image']['type'];
                        finfo_close($finfo);
                        $null = NULL;

                        /*****************    Makes sure file is an image of proper type    *****************/
                                                                                                            //
                                    $extArr = array('png', 'jpg','jpeg','gif');                             //
                                    if(!in_array($extension, $extArr))                                      //
                                    {                                                                       //
                                        echo "<script>"                                                     //
                                        ."alert('File Format Must Be png, jpeg/jpg, or gif');"              //
                                        ."</script>";                                                       //
                                        header("location: manageEmployees.php");                            //
                                        die();                                                              //
                                    }                                                                       //
                        /************************************************************************************/

                        $sql = $conn->prepare("UPDATE STAFF SET S_IMAGE = ?, S_MIME = ? WHERE S_ID = ?");
                        $sql->bind_param("bsi", $null, $mime, $empID);
                        $sql->send_long_data(0, $image);
                        $sql->execute();
                        if(!$result)                                                                                                //
                        {                                                                                                           //
                            die("<script>alert('employee UPDATE Failed.')</script>");                                               //
                        }                                                                                                           //
                        $sql->fetch();
                        $sql->close();
                    }
                    /*********************************************   Handle the BIO ************************************************/
                    $sql = $conn->prepare("UPDATE PORTFOLIO SET PO_DESC = ? WHERE S_ID = ?");                                     //
                    $sql->bind_param("si", $bio, $empID);                                                                       //
                    $result = $sql->execute();                                                                                  //
                                                                                                                                //
                    /*$recievedSID = mysqli_fetch_assoc($conn->query("SELECT S_ID FROM STAFF WHERE "                            //
                    ."S_UNAME = '{$_POST['username']}'AND S_PWORD = '{$_POST['password']}'"))['S_ID'];                          //
                    $result = $conn->query("INSERT INTO PORTFOLIO (S_ID, PO_DESC) VALUES ('$recievedSID', '{$_POST['bio']}')"); */
                    if(!$result)                                                                                                //
                    {                                                                                                           //
                        // Notify user that employee creation failed                                                            //
                        echo                                                                                                    //
                        "<script>
                            displayModal();
                            setText('The employee bio was not updated.<p>Would you like to try again?</p>');
                        </script>";                                                                                             //
                    }
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
/************************************************************************************************************************************/

                    echo "<script>confirmEmpUpdate()</script>";
            }
            else if(isset($_POST['firstVisit']))
            {
                // Do nothing yet
            }
            else
            {   
                // Print out error message
                echo "<script>
                    alert('All Fields Must Be Filled!');
                </script>";
            }

            print "
            
            <nav>
                <ul id='ulNav'>
                    <li id='liNav' style='float:left; background-color:#120fbf;'><a id='aNav' href = 'home.php' >Home</a></li>
                    <li id='liNav' style='float:left; background-color:#120fbf; border-right:none;'><a id='aNav' href = 'manageWebsite.php' >Main Menu</a></li>
                    <li id='liNav'><a id='aNav' href = 'managePortfolio.php' >My Portfolio</a></li>
                    <li id='liNav'><a id='aNav' href = 'managePosts.php' >Blog</a></li>
                    <li id='liNav'><a id='aNav' href = 'manageMerchandise.php' >Merchandise</a></li>
                    <li id='liNav'><a id='aNav' href = 'manageOrders.php' >Orders</a></li>
                    <li id='liNav' style='border-right:none;'><a id='aNav' href = 'manageEmployees.php' >Employees</a></li>
                    <li id='liNav' style='float:right; background-color:#120fbf;'><a id='aNav' href='logout.php'>Logout</a></li>
                </ul>
            </nav>

            <div align='center' id='wrapper'>
                <h1>Update Employee</h1>
                <form action = 'updateEmployee.php' method = 'post' enctype = 'multipart/form-data' name='updateForm'>
                <input type='hidden' class='inputField' name='sID' value='{$_POST['sID']}' readonly>
                <input type='hidden' class='inputField' name='parent' value='{$_POST['parent']}' readonly>
                    <table>
                        <tr>
                            <td>
                                <input type='text' class='inputField' name='name' placeholder='John Doe (First and last name)' value='".(isset($_POST['name']) ? $_POST['name'] : null)."' required>
                            </td>
                            <td>
                                <input type='text' class='inputField' name='username' placeholder='Username' value='".(isset($_POST['username']) ? $_POST['username'] : $u_name)."' required>
                            </td>
                            <td>
                                <input type='password' class='inputField' name='pword' placeholder='Password' value='".(isset($_POST['pword']) ? $pword : null)."' required>
                            </td>
                            
                        </tr>
                        <tr>
                            <td>
                                <input type='email' class='inputField' name='email' placeholder ='Email' value='".(isset($email) ? $email : null)."' required>
                            </td>
                            <td>
                                <input type='tel' class='inputField' name='phone' placeholder='Phonenumber' value='".(isset($phone) ? $phone : null)."' required>
                            </td>
                            <td>
                                <select name='position' class='inputField' style='width:100%;' required>
                                    <option value='Artist' ".($position == 'Artist' ? 'selected' : '').">Artist</option>
                                    <option value='Manager' ".($position == 'Manager' ? 'selected' : '').">Manager</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td colspan='3'>
                                <textarea name='bio' value='".(isset($bio) ? $bio : null)."' maxlenth='1000' style='width:100%; height:100px; resize:none;' placeholder='Tell us a little bit about yourself' required>".(isset($bio) ? $bio : null)."</textarea>
                            </td>
                        </tr>
                    </table>

                    <input type='file' name='image' id='image' onchange='changeLbl()' />
                    <label for='image' id='fileUploader'>Choose an image...</label>

                    <input class='button' type='button' value='Update Employee' name='update' onclick='displayModalForm(this.form)'>
                    <a href='manageEmployees.php'><input type='button' class='delbutton' value='Back' /></a>
                </form>
            </div>";

            $conn->close(); // Close the connection to the database
        }
        else
        {
            // User does not have access to this page. Redirect elsewhere
            header("location: invalid.php");
            die();
        }
    ?>
</html>