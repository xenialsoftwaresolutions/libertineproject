
<html>
    <?php
/********************************************************************************************************************************************
 *                                              php written by: Michael Barfuss   Date: March 10, 2018                                      *
 *                                                HTML written by: Gordon Hann  Date: March 10 2018                                         *
 ********************************************************************************************************************************************/

        require_once ('checkValidUser.php'); // contains function to validate user (returns bool)
        require_once ('connect.php');        // Contains function to connect to database (returns mysqli connection object)
        require_once "sessionTimer.php";
        session_start();
        $reqAccArr = array("Artist","Manager","Pleb");  // array containing required staff positions to access this page

        sessionTimer();

        /******************************************************************
        * Code found on stack overflow to get proper date and time format *
        *******************************************************************/
        function getDatetimeNow()   // returns the date and time in the format SQL accepts
        {
            $tz_object = new DateTimeZone('America/Edmonton');
            //date_default_timezone_set('America/Edmonton'); // alternate method

            $datetime = new DateTime();
            $datetime->setTimezone($tz_object);
            return $datetime->format('Y\-m\-d\ h:i:s');
        }

        if(checkValidUser($_SESSION['accessLVL'], $reqAccArr, '')) // make sure user is authorized
        {


            // if no Post ID, return to managePosts page
            if(!isset($_POST['pID']))
            {
/****************************************************************************************************** If File Not Selected, runs this code??! ******/
                header("location: managePosts.php");
                die();
            }else
            {
                $postID = $_POST['pID'];
            }

            /***************    Get title and content of post    ************************/
            $title = $content = "";                                         //////////////
            if(isset($_POST['title']))                                      //////////////
            {                                                               //////////////
                $title = $_POST['title'];                                   //////////////
            }                                                               //////////////
                                                                            //////////////
            if(isset($_POST['content']))                                    //////////////
            {                                                               //////////////
                $content = $_POST['content'];                               //////////////
            }                                                               //////////////
             /***************************************************************************/

            $sID = $_SESSION['sID']; // Staff member ID retrieved at login
            $portfolioID;

			
			$conn = connect();  // connect to database

            // checks the page that called this one. If no parent, return to manageWebsite
            if(isset($_POST['parent']))
            {
                /*****************************************************************************/
                // Assign 0 or the ID associated with the logged in staff member's portfolio //
                /*****************************************************************************/
                if ($_POST['parent'] == "managePost.php")
                {
                    $portfolioID = 0;
                }
                else
                {
                    $sql = $conn->prepare("SELECT PO_ID FROM portfolio WHERE S_ID = ?;");
                    $sql->bind_param("i", $sID);
                    $sql->execut();
                    $sql->bind_result($recievedPO_ID);
                    $sql->fetch();
                    $sql->close();
                    $portfolioID = $recievedPO_ID;
                }
            }
            else
            {
                header("location: manageWebsite.php");  // weren't directed here by valid page
                die();
            }

            /*************    Evaluates whether required fields are filled in    **************/
            //                                                                              ////
            if(isset($_POST['content'], $_POST['title']))                                   ////
            {                                                                               ////
                $fieldsFilled = true;                                                       ////
            }else                                                                           ////
            {                                                                               ////
                $fieldsFilled = false;                                                      ////
            }                                                                               ////
            /**********************************************************************************/
            
            // if form submitted: do nothing on initial load,
            // check filled fields, error message not all fields filled
            if (isset($_POST['update']))
            {   
                // if fields filled, update post
	            if (!empty($fieldsFilled) && $fieldsFilled === TRUE)
                {	
                    $currTime = getDatetimeNow();
/***************************************************    Update Post Table    **********************************************************/
                    $zero = 0;
                    $sql = $conn->prepare("UPDATE POST SET S_ID = ?, P_TITLE = ?, P_DATE = ?, P_CONTENT = ?, PO_ID = ?, P_DELETED = ?"
                    ."WHERE P_ID = ?;");
                    $sql->bind_param("isssii", $sID, $title, $currTime, $content, $portfolioID, $zero, $postID);
                    $result = $sql->execute();
                    $sql->close();

                    /*$result = $conn->query("UPDATE POST SET S_ID ='$sID', P_TITLE = '$title',"                                      //
                    ."P_DATE = '$currTime', P_CONTENT = '$content', PO_ID = '$portfolioID', P_DELETED = 0 WHERE P_ID = '$postID'");*/ //
                                                                                                                                      //
                    if(!$result)                                                                                                      //
                    {                                                                                                                 //
                        die("post UPDATE Failed.");                                                                                   //
                    }                                                                                                                 //
/**************************************************************************************************************************************/

                }else if(isset($_POST['firstVisit']))
                {
                    // Do nothing yet
                }
                else
                {   
                    // Print out error message
	                echo "<script>
                        alert('All Fields Must Be Filled!');
                    </script>";
                }
            }

/****************************    Obtain data to populate form    ********************************/
            $sql = $conn->prepare("SELECT IMG_ID FROM POST_IMAGE WHERE P_ID = ?;");             //
            $sql->bind_param("i", $postID);                                                     //
            $sql->execute();                                                                    //
            $result = $sql->get_result();                                                       //
            //$IMG_ID = $conn->query("SELECT IMG_ID FROM POST_IMAGE WHERE P_ID = '$postID'");   //
            $IMG_ID = mysqli_fetch_assoc($result);                                              //
            $IMG_ID = $IMG_ID['IMG_ID'];                                                        //
                                                                                                //
            $sql = $conn->prepare("SELECT * FROM IMAGE WHERE IMG_ID = ?;");                     //
            $sql->bind_param("i", $IMG_ID);                                                     //
            $sql->execute();                                                                    //
            $imageResults = $sql->get_result();                                                 //
            $sql->fetch();                                                                      //
            $sql->close();                                                                      //
			/*$sql = "SELECT * FROM IMAGE WHERE IMG_ID = '".$IMG_ID['IMG_ID']."'";              //
            $imageResults = $conn->query($sql);*/                                               //
                                                                                                //
            $sql = $conn->prepare("SELECT * FROM POST WHERE P_ID = ?;");                        //
            $sql->bind_param("i", $postID);                                                     //
            $sql->execute();                                                                    //
            $postResult = $sql->get_result();                                                   //
            $sql->fetch();                                                                      //
            $sql->close();                                                                      //
            //$postResult = $conn->query("SELECT * FROM POST WHERE P_ID = '$postID'");          //
                                                                                                //
			$row = mysqli_fetch_assoc($imageResults);                                           //
            $post = mysqli_fetch_assoc($postResult);                                            //
/************************************************************************************************/

            print "
            <title> Libertine Tattoo - Update Post</title>
         
            <form action = 'updatePost.php' method = 'post' enctype = 'multipart/form-data'>
                <input type = 'text' name = 'title' value = '".$post['P_TITLE']."'/> <br />
                <input type = 'text' name = 'content' value = '".$post['P_CONTENT']."'/> <br />
                <input type = 'text' name = 'parent' value = '".$_POST['parent']."' hidden readonly />
                <input type = 'text' value = '".$postID."' name = 'pID' hidden readonly />
                <image src = 'data:".$row['IMG_MIME'].";base64,".base64_encode($row['IMG_DATA'])."'></image><br />
                <input type = 'submit' value ='BACK' name = 'back' formaction = 'managePostas.php' />
                <input type = 'submit' value ='UPDATE POST' name = 'update' /> 
            </form>
            ";

            $conn->close(); // Close the connection to the database
        }
        else
        {
            // User does not have access to this page. Redirect elsewhere
            header("location: invalid.php");
            die();
        }
    ?>
</html>