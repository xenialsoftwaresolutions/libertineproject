// Display the modal while grabbing the form as an object
// used on managePosts.php to grab a form to delete it.
function displayModalForm(formObj)
{
    var modal = document.getElementById('myModal');
    modal.style.display = "block";
    thisForm = formObj;
}

// JS DOES NOT SUPPORT OVERLOADING
// This function displays the modal without grabbing a form object
function displayModal()
{
    var modal = document.getElementById('myModal');
    modal.style.display = "block";
}

// Set the text of the modal
// middle content of the modal a <p> tag id=modal-text
function setText(str)
{
    var modalText = document.getElementById('modal-text');
    modalText.innerHTML = str;
}

function closeModal()
{
    var modal = document.getElementById('myModal');
    modal.style.display = "none";
}

// change the action of the yes button on the modal
function confirmClick(url)
{    
    window.location.replace(url);
}

// change the action of the no button on the modal
function cancelClick(url)
{
    window.location.replace(url);
}

// change the action of the yes button on the modal
// used with a form to submit
function confirmClickForm(url)
{    
    thisForm.action = url;
    thisForm.submit();
    thisForm = null;
}

// change the action of the no button on the modal
// used with a form
function cancelClickForm(url)
{
    thisForm.action = url;
    thisForm.submit();
    thisForm = null;
}

function confirmEmpUpdate()
{
    var yesButt = document.getElementById('yesButt');
    var noButt = document.getElementById('noButt');

    yesButt.innerText = "Ok";
    yesButt.onclick =  function() { confirmClick("manageEmployees.php"); }
    noButt.hidden = true;

    setText("Successfully Updated Employee.");
    displayModal();
}

function closeAndSubmit(url)
{
    var modal = document.getElementById('myModal');
    modal.style.display = "none";

    thisForm.action = url;
    thisForm.submit();
    thisForm = null;
}

function noClickReturn(url)
{
    var continueCreating = document.getElementById("continueCreating");
    continueCreating.value = "false";

    var modal = document.getElementById('myModal');
    modal.style.display = "none";

    thisForm.action = url;
    thisForm.submit();
    thisForm = null;
}