<?php

function generateSalt() 
{
    return substr(str_shuffle("1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWRXYZ"), 0, 5);
}

?>