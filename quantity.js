

function pricingUpdate()
{
    GST();
    TOTAL();
    SUBTOTAL();
}

function GST()
{
    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            document.getElementById('gst').innerHTML = this.responseText;
        }
    };
    xhttp.open("GET", "calcGST.php", true);
    xhttp.send();
}

function TOTAL()
{
    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            document.getElementById('total').innerHTML = this.responseText;
        }
    };
    xhttp.open("GET", "calcTOTAL.php", true);
    xhttp.send();
}

function SUBTOTAL()
{
    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            document.getElementById('subtotal').innerHTML = this.responseText;
        }
    };
    xhttp.open("GET", "calcSUBTOTAL.php", true);
    xhttp.send();
}

function upClickAJAX(buttonID, ID, size)
{
    var quantity = document.getElementById(buttonID);

    var tempQuant = parseInt(quantity.value) + 1;
    var xhttp = new XMLHttpRequest();


    xhttp.onreadystatechange = function()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            quantity.value = this.responseText;
            pricingUpdate();
        }
    };
    xhttp.open("GET", "modQuantity.php?q="+tempQuant+"&itemID="+ID+"&size="+size, true);
    xhttp.send();
}

function downClickAJAX(buttonID, ID, size)
{
    var quantity = document.getElementById(buttonID);
    if (quantity.value > 0)
    {
        var tempQuant = quantity.value - 1;
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function()
        {
            if (this.readyState == 4 && this.status == 200)
            {
                quantity.value = this.responseText;
                pricingUpdate();
            }
        };
        xhttp.open("GET", "modQuantity.php?q="+tempQuant+"&itemID="+ID+"&size="+size, true);
        xhttp.send();
    }
}