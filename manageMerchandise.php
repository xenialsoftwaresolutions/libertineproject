<html>
    <head>
        <link rel='stylesheet' href='management.css'>
        <link rel='stylesheet' href='manageMerch.css'>
        <link rel='stylesheet' href='modal.css'>
        <script src='modal.js'></script>
        <title>Libertine Tattoo - Orders</title>
    </head>
    <body>
        <div id='myModal' class='modal'>
            <!-- Modal content -->
            <div class='modal-content'>
                <div class='modal-header'>
                    <span type='button' class='close' onclick='return closeModal()'>&times;</span>
                    <h2>Libertine Tattoo</h2>
                </div>
                <div class='modal-body'>
                    <p id='modal-text'>Are you sure you want to delete this item? <br /><br /> WARNING: Deleting this item will delete the record and all associated data!</p>
                    <button type='button' class='modal-butt' id='noButt' onclick='closeModal()'>No</button>
                    <button type='button' class='modal-butt' id='yesButt' onclick='confirmClickForm("deleteItem.php")'>Yes</button>
                </div>
            </div>
        </div>

        <nav>
            <ul id='ulNav'>
                <li id='liNav' style='float:left; background-color:#120fbf;'><a id='aNav' href = 'home.php' >Home</a></li>
                <li id='liNav' style='float:left; background-color:#120fbf; border-right:none;'><a id='aNav' href = 'manageWebsite.php' >Main Menu</a></li>
                <li id='liNav'><a id='aNav' href = 'managePortfolio.php' >My Portfolio</a></li>
                <li id='liNav'><a id='aNav' href = 'managePosts.php' >Blog</a></li>
                <li id='liNav'><a id='aNav' href = 'manageMerchandise.php' >Merchandise</a></li>
                <li id='liNav'><a id='aNav' href = 'manageOrders.php' >Orders</a></li>
                <li id='liNav' style='border-right:none;'><a id='aNav' href = 'manageEmployees.php' >Employees</a></li>
                <li id='liNav' style='float:right; background-color:#120fbf;'><a id='aNav' href='logout.php'>Logout</a></li>
            </ul>
        </nav>
        <?php
            // Chris Toth March 22, 2018

            session_start();

            require_once('connect.php');
            require_once('checkValidUser.php');
            require_once "sessionTimer.php";

            // determines access level of the current page based on logged in users position
            $accessArray = array("Artist", "Manager");

            sessionTimer();

            // compares the users position (accessLVL) to an array of employees allowed to access this page.
            // the third string is a special case for manageWebsite main menu page
            if (checkValidUser($_SESSION['accessLVL'], $accessArray, ""))
            {
            $conn = connect(); // Connect to database

            $sql = $conn->query("SELECT M_ID, M_NAME, M_PRICE FROM MERCH_ITEM WHERE M_DELETED='0';");
            
            if ($conn->error)
            {
                echo $conn->error;
                $conn->close();
                die();
            }

            echo "
            <div align ='center' id='wrapper'>
                <h1>Manage Merch</h1>
                <table>
                    <tr>
                        <th>Item ID</th>
                        <th>Item Name</th>
                        <th>Order Cost</th>
                        <th colspan='2'>Actions</th>
                    </tr>";
            while ($row = mysqli_fetch_assoc($sql))
            {
                echo "
                <form action = '' method='post' enctype='multipart/form-data'>
                    <tr>
                        <td>
                            <input type='text' id='mID' value='{$row['M_ID']}' name='mID' class='inputField' readonly>
                        </td>
                        <td>
                            <input type='text' value='{$row['M_NAME']}' name='mNAME' class='inputField' readonly>
                        </td>
                        <td>
                            <input type='text' value='$" . number_format(($row['M_PRICE'] / 100), 2). "' name='mPRICE' class='inputField' readonly>
                        </td>
                        <td>
                            <input type='submit' value='Update' name='update' class='button' formaction='updateItem.php'>
                        </td>
                        <td>
                            <input type='button' value='Delete Item' name='delete' class='delbutton' onclick='return displayModalForm(this.form)'>
                    </tr>
                </form>
                ";
            }
            echo "</table></div>";

            echo "<a href='addItem.php' id='createButt'>Add New Item</a>";
            }
            else
            {
                // User does not have access to this page. Redirect elsewhere
                header("location: invalid.php");
                die();
            }
        ?>
    </body>
</html>