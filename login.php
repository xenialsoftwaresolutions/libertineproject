<?php
    // Chris Toth - March 10, 2018
    require('connect.php');

    // start the session is required to continue using the logged in users info
    session_start();

    // connect to the database using the function within connect.php
    $conn = connect();

    // show variable initialized to none. this is the displayed message when the user does not enter their credentials properly
    $show = "none";

    // if the username and password fields are entered in the form continue.
    if(isset($_POST['username']) && isset($_POST['password']))
    {
        // grab username and password from the fields
        $username = $_POST['username'];
        $password = $_POST['password'];

        // make the SQL string. this is used to query the database for the user
        // hopefully only one user is returned from the query
        $sql = $conn->prepare("SELECT S_NAME, S_UNAME, S_PWORD, S_ID, S_POSITION, S_SALT FROM STAFF WHERE S_UNAME= ?;");
        
        // if there was an error print it, close connection, and die
        if (!$sql)
        {
            echo "Error:<br />" . $conn->error ." ".$conn->errno. "<br />";
            $sql->close();
            $conn->close();
            die();
        }
        $sql->bind_param("s", $username);
        $sql->execute();
        $result = $sql->get_result(); // note - this requires the mysqlnd driver

        /*$sql = "SELECT S_NAME, S_PWORD, S_ID, S_POSITION FROM staff WHERE S_NAME='$username'";
        $result = $conn->query($sql);*/
        

        while ($record = mysqli_fetch_assoc($result))
        {
            $password = $password . $record['S_SALT'];
            $password = hash("sha256", $password, false);
            // if the returned record from the query is EXACTLY equal to the entered information
            if ($record['S_UNAME'] == $username && $record['S_PWORD'] == $password) 
            {
                // start the session varaiables for later use
                $_SESSION['S_NAME'] = explode(" ", $record['S_NAME'])[0];
                $_SESSION['accessLVL'] = $record['S_POSITION'];
                $_SESSION['sID'] = $record['S_ID'];
    
                //Assign the current timestamp as the user's
                //latest activity
                $_SESSION['last_action'] = time();
                
                // forward the user on to manage the website and die
                header("location: manageWebsite.php");
                die();
            }
        }
        $result->free();
        $sql->fetch();
        $sql->close();
        $conn->close();
        if(!isset($_SESSION['S_NAME'], $_SESSION['S_POSITION'], $_SESSION['S_ID']))
        {
            // when the user does not enter the credentials correctly.
            // display the header with the message as 'block' instead of 'none'
            // to let the user know their input is incorrect.
            $show = "block";
          }
    }

    // no user is logged in print the page and the form.
    if (empty($_SESSION['S_NAME']))
    {
        echo "
            <!doctype html>

                <head>
                    <script src='jquery.min.js'></script>
                    <link rel='stylesheet' href='login.css'>
                    <link rel='stylesheet' href='management.css'>
                    <title> Libertine Tattoo - Login</title>
                </head>
                <body>
                    <div id='loginBox'> 
                        <form action='login.php' method='post'>

                            <h1 class='header' style='font-size:65px;'>Libertine</h1>

                            <input class='credentials' type='text' name='username' placeholder='username' required />

                            <input class='credentials' type='password' name='password' placeholder='password' required />

                            <div style='text-align:center;'>
                                <input class='credButt' type='submit' name='submit' value='Login' />
                            </div>

                            <div style='text-align:center;'>    
                                <a class='hyperlink' style='text-align:left;' href='home.php'>Back</a>
                                <!--<a class='hyperlink' style='text-align:right;' href='home.php'>Forgot Password?</a>-->
                            </div>

                            <h3 id='errorMsg' style='color:red; text-align:center; display:{$show};'>The credentials you have entered do not match our records!</h3>

                        </form>
                    </div>
                </body>  
            </html>   
        ";
    }
    else // else the user is already logged in...
    {
        header("location: manageWebsite.php"); //redirect to manage site
        die();
    }

?>