<?php
    // Chris Toth March 10, 2018
    //
    // logout script is called when the user clicks the logout button.
    // the session variables are all destroyed and the user is redirected to the home page.
    session_start();
    session_destroy();
    header("location: home.php");
?>