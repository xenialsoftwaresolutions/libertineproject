<?php
    require_once ('connect.php');
    require_once "checkValidUser.php";
    require_once "sessionTimer.php";
    session_start();

    // determines access level of the current page based on logged in users position
    $reqAccArr = array("Artist", "Manager");

    sessionTimer();

    // compares the users position (accessLVL) to an array of employees allowed to access this page.
    // the third string is a special case for manageWebsite main menu page  
    if(checkValidUser($_SESSION['accessLVL'], $reqAccArr, ""))
    {
        $conn = connect();
        
        // get neccessary variables
        $mID = $_POST['mID'];
        

        // update order table to mark an order as complete
        $sql = $conn->query("UPDATE MERCH_ITEM SET M_DELETED='1' WHERE M_ID='{$mID}';");

        if($conn->error)
        {
            die($conn->error . "\npost UPDATE Failed. ln 80");
        }
        else
        {
            $conn->close();
            header("location: manageMerchandise.php");
            die();
        }
    }
    else
    {
        // User does not have access to this page. Redirect elsewhere
        header("location: invalid.php");
        die();
    }    
?>