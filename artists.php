<script>

    function indivArtistPage(name)
    {
        document.theForm.action = 'portfolio.php';
        document.getElementById("name").value = name;
        document.theForm.submit();
    }

</script>

<?php
require_once("printHTML.php");
require ('connect.php');
$conn = connect();

print"
<form name='theForm' method='GET' enctype='multipart/form-data'>
    <input type='text' name='name' id='name' value='' hidden>
</form>";

echo "

<!doctype html>

    <head>
        <script src='jquery.min.js'></script>
        <script src='loadingscreen.js'></script>
        <link rel='stylesheet' href='loader.css'>
        <link rel='stylesheet' href='style.css'>
        <title> Libertine Tattoo - Artists</title>
    </head>

<body>";

printLoader();
printNav();
echo"



<div id ='artphoto' class='tile'>
            <div class='center'>Artists</div>
        </div>"; 

$result = $conn->query("SELECT * FROM STAFF WHERE S_POSITION = 'Artist' AND S_DELETED = 0");

$sql = $conn->prepare("SELECT S_IMAGE FROM STAFF WHERE S_ID = ?;");

while($artist = mysqli_fetch_assoc($result))
{
    $id = $artist['S_ID'];
    $sql->bind_param("i", $id);
    $sql->execute();
    $sql->store_result();
    $sql->bind_result($image);
    $sql->fetch();
    
print"
<div class='hovertext' onclick='indivArtistPage(\"".$artist['S_NAME']."\")'>
    <image src = 'data:".$artist['S_MIME'].";base64,".base64_encode($image)."' />
    <h2>".$artist['S_NAME']."</h2>
</div>";
}
$sql->close();
$conn->close();

        printFooter();
       echo"
        
        

      <button onclick='topFunction()' id='myBtn' title='Go to top'>Top</button>
          
    
        <script>

            window.onscroll = function() {scrollFunction()};

            function scrollFunction() {
                if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                    document.getElementById('myBtn').style.display = 'block';
                } else {
                    document.getElementById('myBtn').style.display = 'none';
                }
            }

            function topFunction() {
                document.body.scrollTop = 0;
                document.documentElement.scrollTop = 0;
            }

        </script>
    </body>
</html>
";

/*print "
<div class='hovertext'>
    <image src = 'images/boat.jpg' />
    <h2>Artist</h2>
</div>


<div class='hovertext'>
    <image src = 'images/hill.jpg' />
    <h2>Artist</h2>
</div>

<div class='hovertext'>
    <image src = 'images/mountain.png' />
    <h2>Artist</h2>
</div>


</body>

</html>";*/


?>