<html>
    <head>
        <link rel='stylesheet' href='management.css'>
        <link rel='stylesheet' href='viewOrder.css'>
        <link rel='stylesheet' href='modal.css'>
        <script src='modal.js'></script>
        <title>Libertine Tattoo - View Order</title>
    </head>
    <body>
        <div id='myModal' class='modal'>
            <!-- Modal content -->
            <div class='modal-content'>
                <div class='modal-header'>
                    <span type='button' class='close' onclick='return closeModal()'>&times;</span>
                    <h2>Libertine Tattoo</h2>
                </div>
                <div class='modal-body'>
                    <p id='modal-text'>Are you sure this order has been fulfilled? <br /><br /> WARNING: Marking this order as fulfilled will delete the record!</p>
                    <button type='button' class='modal-butt' id='noButt' onclick='closeModal()'>No</button>
                    <button type='button' class='modal-butt' id='yesButt' onclick='confirmClickForm("deleteOrder.php")'>Yes</button>
                </div>
            </div>
        </div>

        <nav>
            <ul id='ulNav'>
                <li id='liNav' style='float:left; background-color:#120fbf;'><a id='aNav' href = 'home.php' >Home</a></li>
                <li id='liNav' style='float:left; background-color:#120fbf; border-right:none;'><a id='aNav' href = 'manageWebsite.php' >Main Menu</a></li>
                <li id='liNav'><a id='aNav' href = 'managePortfolio.php' >My Portfolio</a></li>
                <li id='liNav'><a id='aNav' href = 'managePosts.php' >Blog</a></li>
                <li id='liNav'><a id='aNav' href = 'manageMerchandise.php' >Merchandise</a></li>
                <li id='liNav'><a id='aNav' href = 'manageOrders.php' >Orders</a></li>
                <li id='liNav' style='border-right:none;'><a id='aNav' href = 'manageEmployees.php' >Employees</a></li>
                <li id='liNav' style='float:right; background-color:#120fbf;'><a id='aNav' href='logout.php'>Logout</a></li>
            </ul>
        </nav>
<?php
    // Chris Toth March 22, 2018

    session_start();

    require_once('connect.php');
    require_once('checkValidUser.php');
    require_once "sessionTimer.php";

    $accessArray = array("Artist", "Manager");

    sessionTimer();

    if (checkValidUser($_SESSION['accessLVL'], $accessArray, ""))
    {
        $conn = connect(); // Connect to database

        if (isset($_POST['oID']))
        {
            $sql = $conn->query("SELECT ORDERS.O_ID, ORDERS.O_PLACE_DATE, ORDERS.O_COST, CUSTOMER.C_NAME, ORDERS.C_ID, CUSTOMER.C_POSTAL_CODE, CUSTOMER.C_PROVINCE, CUSTOMER.C_ADDRESS, CUSTOMER.C_CITY, CUSTOMER.C_EMAIL FROM ORDERS INNER JOIN CUSTOMER ON ORDERS.C_ID = CUSTOMER.C_ID WHERE ORDERS.O_ID='{$_POST['oID']}';");
            
            if ($conn->error)
            {
                echo $conn->error;
                $conn->close();
                die();
            }

            $row = mysqli_fetch_assoc($sql);
            echo "
            <div align ='center' id='wrapper' style='background-color:gray;'>
                <div id='orderPage'>
                    <table cellpadding='10' width='100%'>
                        <tr>
                            <td colspan='4'>
                                <h1 class='orderTitle'>Order #{$row['O_ID']}</h1>
                            </td>
                        </tr>
                        <tr>
                            <td colspan='4'>
                                <h2 class='orderTitle-small'>Customer Details</h2>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <legend>Customer Name</legend>
                                <h3>{$row['C_NAME']}</h3>
                            </td>
                            <td>
                                <legend>Customer Email</legend>
                                <h3>{$row['C_EMAIL']}</h3>
                            </td>
                        </tr>
                            <td>
                                <legend>Street Address</legend>
                                <h3>{$row['C_ADDRESS']}</h3>  
                            </td>
                            <td>
                                <legend>City</legend>
                                <h3>{$row['C_CITY']}</h3>
                            </td> 
                            <td>
                                <legend>Province</legend>
                                <h3>{$row['C_PROVINCE']}</h3>
                            </td>
                            <td>
                                <legend>Postal Code</legend>
                                <h3>{$row['C_POSTAL_CODE']}</h3>
                            </td>
                        </tr>
                        <tr>
                            <td colspan='4'>
                                <h2 class='orderTitle-small'>Order Contents</h2>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <legend>Item ID</legend>
                            </td>
                            <td>
                                <legend>Item Name</legend>
                            </td>
                            <td>
                                <legend>Item Price</legend>
                            </td>
                            <td>
                                <legend>Quantity Ordered</legend>
                            </td>
                        </tr>
                        ";

                        $merch_sql = $conn->query("SELECT merch_item.M_ID, merch_item.M_NAME, merch_item.M_PRICE, merch_order.QUANTITY FROM merch_item INNER JOIN merch_order ON merch_order.M_ID = merch_item.M_ID WHERE merch_order.O_ID = {$row['O_ID']};");
                        
                        if ($conn->error)
                        {
                            echo $conn->error;
                            $conn->close();
                            die();
                        }
                        
                        while ($item = mysqli_fetch_assoc($merch_sql))
                        {
                            echo "
                            <tr>
                                <td>
                                    <h3>{$item['M_ID']}</h3>
                                </td>
                                <td>
                                    <h3>{$item['M_NAME']}</h3>
                                </td>
                                <td>
                                    <h3>$" . number_format(($item['M_PRICE'] / 100), 2). "</h3>
                                </td>
                                <td>
                                    <h3>{$item['QUANTITY']}</h3>
                                </td>
                            </tr>";
                        }
                        
                        echo"
                        <tr>
                            <td colspan='4'>
                                <h2 class='orderTitle-small'>Order Info</h2>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <legend>Order Date</legend>
                                <h3>{$row['O_PLACE_DATE']}</h3>
                            </td>
                            <td>
                                <legend>Order Total Cost</legend>
                                <h3>$" . number_format(($row['O_COST'] / 100), 2). "</h3>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href='manageOrders.php' class='button'>Back</a>
                            </td>
                            <td colspan='3' align='right'>
                                <form action = '' method='post' enctype='multipart/form-data'>
                                    <input type='text' name='oID' value='{$row['O_ID']}' hidden readonly />
                                    <input type='button' value='Mark as Fulfilled' name='mark' class='button' onclick='return displayModalForm(this.form)'>
                                </form>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            
        ";
        }
    }
    else
    {
        // User does not have access to this page. Redirect elsewhere
        header("location: invalid.php");
        die();
    }
?>