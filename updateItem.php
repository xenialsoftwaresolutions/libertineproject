<html>
    <head>
        <link rel='stylesheet' href='management.css'>
        <link rel='stylesheet' href='modal.css'>
        <link rel='stylesheet' href='updateItem.css'>
        <script src='modal.js'></script>
        <script src='changeLbl.js'></script>
        <title>Libertine Tattoo - Orders</title>
    </head>
    <body>
        <div id='myModal' class='modal'>
            <!-- Modal content -->
            <div class='modal-content'>
                <div class='modal-header'>
                    <span type='button' class='close' onclick='return closeModal()'>&times;</span>
                    <h2>Libertine Tattoo</h2>
                </div>
                <div class='modal-body'>
                    <p id='modal-text'>The item was successfully updated! <br /><br /> Would you like to continue editing this record?</p>
                    <button type='button' class='modal-butt' id='noButt' onclick='cancelClick("manageMerchandise.php")'>No</button>
                    <button type='button' class='modal-butt' id='yesButt' onclick='closeModal()'>Yes</button>
                </div>
            </div>
        </div>

        <nav>
            <ul id='ulNav'>
                <li id='liNav' style='float:left; background-color:#120fbf;'><a id='aNav' href = 'home.php' >Home</a></li>
                <li id='liNav' style='float:left; background-color:#120fbf; border-right:none;'><a id='aNav' href = 'manageWebsite.php' >Main Menu</a></li>
                <li id='liNav'><a id='aNav' href = 'managePortfolio.php' >My Portfolio</a></li>
                <li id='liNav'><a id='aNav' href = 'managePosts.php' >Blog</a></li>
                <li id='liNav'><a id='aNav' href = 'manageMerchandise.php' >Merchandise</a></li>
                <li id='liNav'><a id='aNav' href = 'manageOrders.php' >Orders</a></li>
                <li id='liNav' style='border-right:none;'><a id='aNav' href = 'manageEmployees.php' >Employees</a></li>
                <li id='liNav' style='float:right; background-color:#120fbf;'><a id='aNav' href='logout.php'>Logout</a></li>
            </ul>
        </nav>
<?php
    /********************************************************************************************************************************************
    *                                              PHP written by: Chris Toth        Date: March 24, 2018                                      *
    *                                             HTML written by: Chris Toth        Date: March 24, 2018                                      *
    ********************************************************************************************************************************************/

    require_once ('checkValidUser.php'); // contains function to validate user (returns bool)
    require_once ('connect.php');        // Contains function to connect to database (returns mysqli connection object)
    require_once "sessionTimer.php";
    session_start();

    $reqAccArr = array("Artist", "Manager");  // array containing required staff positions to access this page

    sessionTimer();


    if(checkValidUser($_SESSION['accessLVL'], $reqAccArr, "")) // checkValidUser returns bool
    {
        // Connect to the database
        $conn = connect();
        
        // Handle the form submission
        if (isset($_POST['save']))
        {
            //values for the merch_item table
            $mID = $_POST['mID'];
            $mNAME = $_POST['name'];
            $mDESC = $_POST['description'];
            // Clean the price input
            //
            // The value is being stored in the database as one integer
            // move the decimal place
            // remove the commas
            $mPRICE = ($_POST['price'] * 100);
            $mPRICE = intval(str_replace(',', '', $mPRICE));
            $mPRICE = intval(str_replace('$', '', $mPRICE));
            $mTYPE = $_POST['type'];

            // get values to input into the inventory table
            $sNONE = $_POST['none'];
            $sXS = $_POST['xs'];
            $sS = $_POST['s'];
            $sM = $_POST['m'];
            $sL = $_POST['l'];
            $sXL = $_POST['xl'];
            $sXXL = $_POST['xxl'];

            // UPDATE MERCH_ITEM TABLE
            $sql = $conn->prepare("UPDATE MERCH_ITEM SET M_NAME = ?, M_DESC = ?, M_PRICE = ?, M_TYPE = ? WHERE M_ID = ?;");
            $sql->bind_param("ssiss", $mNAME, $mDESC, $mPRICE, $mTYPE, $mID);
            $result = $sql->execute();
            $sql->fetch();
            $sql->close();

            if(!$result)
            {
                die($sql->error . $sql->errno);
            }

            if(isset($_FILES['image']))
            {
                // formats image for database
                $image = file_get_contents($_FILES['image']['tmp_name']);
                $extension = strtolower(substr($_FILES['image']['name'], strrpos($_FILES['image']['name'], '.') + 1));
                $finfo = finfo_open(FILEINFO_MIME_TYPE);
                $mime = $_FILES['image']['type'];
                finfo_close($finfo);

    /*****************    Makes sure file is an image of proper type    *****************/
                                                                                        //
                $extArr = array('png', 'jpg','jpeg','gif');                             //
                if(!in_array($extension, $extArr))                                      //
                {                                                                       //
                    echo "<script>"                                                     //
                    ."alert('File Format Must Be png, jpeg/jpg, or gif');"              //
                    ."</script>";                                                       //
                    header("location: addItem.php");                                    //
                    die();                                                              //
                }                                                                       //
    /************************************************************************************/
                                                            
                $imageExists = FALSE; 
                $images = $conn->query("SELECT * FROM IMAGE");

    /***********    Checks if the recieved image already exists in the database    **********/
                while($row1 = mysqli_fetch_assoc($images))                                  //
                {                                                                           //
                    if($row1['IMG_DATA'] == $image && $row1['IMG_MIME'] == $mime            //
                    && $row1['IMG_EXT'] == $extension)                                     //
                    {                                                                       //
                        $imageID = $row1['IMG_ID'];                                         //
                        $image = $row1['IMG_DATA'];                                         //
                        $imageExists = TRUE;                                                //
                        break;                                                              //
                    }                                                                       //
                }

                if ($imageExists === FALSE)
                {
                    // If image does not exist. add it.
                    $imgINSERT = $conn->prepare("INSERT INTO IMAGE (IMG_DATA, IMG_MIME, IMG_EXT) VALUES (?, ?, ?);");
                    $imgINSERT->bind_param("bss", $null, $mime, $extension);
                    $imgINSERT->send_long_data(0, $image);
                    $result = $imgINSERT->execute();
                    $imgINSERT->close();

                    if(!$result)
                    {
                        // If query fails output error message and code
                        die($imgINSERT->error ."<br />". $imgINSERT->errno);
                    }
                }

                /**************************************    Get ID's to insert into MERCH_IMAGE    ********************************/
                $imgID_SQL = $conn->prepare("SELECT IMG_ID FROM IMAGE WHERE IMG_DATA = ? AND IMG_EXT = ?;");                    //
                $imgID_SQL->bind_param("bs", $null, $extension);                                                                //
                $imgID_SQL->send_long_data(0, $image);                                                                          //
                $imgID_SQL->execute();                                                                                          //
                $imgID_SQL->bind_result($imageID);                                                                              //
                $imgID_SQL->fetch();                                                                                            //
                $imgID_SQL->close();

                $merchID_SQL = $conn->query("SELECT MAX(M_ID) FROM MERCH_ITEM;");
                $m_id = mysqli_fetch_assoc($merchID_SQL)['MAX(M_ID)'];
        
                if(!$result)
                {
                    die($sql->error . $sql->errno);
                }

                $merchIMG_SQL = $conn->prepare("UPDATE MERCH_IMAGE SET IMG_ID = ? WHERE M_ID = ?;");
                $merchIMG_SQL->bind_param("ii", $imageID, $m_id);
                $merch_image_result = $merchIMG_SQL->execute();
                $merchIMG_SQL->fetch();
                $merchIMG_SQL->close();

                if(!$merch_image_result)
                {
                    die("failed to update the image!");
                }
        }

            // UPDATE INVENTORY TABLE
            $sql2 = $conn->prepare("UPDATE INVENTORY SET NA = ?, XS = ?, S = ?, M = ?, L = ?, XL = ?, XXL = ? WHERE M_ID = ?;");
            $sql2->bind_param("iiiiiiii", $sNONE, $sXS, $sS, $sM, $sL, $sXL, $sXXL, $mID);
            $result2 = $sql2->execute();
            $sql2->close();

            if(!$result2)
            {
                die($sql2->error . $sql2->errno);
            }

            // prompt success message
            echo "<script>displayModal();</script>";
        }
        // Retrieve necessary items from submitted form
        $mID = $_POST['mID'];        

        // Select the indicated row to be displayed from MERCH_ITEM
        $sql = $conn->prepare("SELECT M_NAME, M_DESC, M_TYPE, M_PRICE FROM MERCH_ITEM WHERE M_ID = ?");
        $sql->bind_param("i", $mID);
        $sql->execute();

        if ($sql->error)
        {
            echo $sql->error;
            $sql->close();
            $conn->close();
            die();
        }
        $resultSQL = $sql->get_result();
        $sql->fetch();
        $sql->close();

        // Select the indicated row to be displayed from MERCH_ITEM
        $sql2 = $conn->prepare("SELECT * FROM INVENTORY WHERE M_ID = ?");
        $sql2->bind_param("i", $mID);
        $sql2->execute();

        if ($sql2->error)
        {
            echo $sql2->error;
            $sql2->close();
            $conn->close();
            die();
        }
        $resultSQL2 = $sql2->get_result();
        $sql2->fetch();
        $sql2->close();

        $record = mysqli_fetch_assoc($resultSQL);
        $sizes = mysqli_fetch_assoc($resultSQL2);
        echo "
         <div align ='center' id='wrapper'>
            <h1>Update Merch</h1>
            <table>
                <form action='' method='post' enctype='multipart/form-data'>
                    <input type='text' name='mID' value='".$mID."' hidden readonly>
                    <tr>
                        <td>
                            <legend>Item Name</legend>
                            <input type='text' class='inputField' id='name' name='name' placeholder='Name' value='".(isset($record['M_NAME']) ? $record['M_NAME'] : null)."' required>
                        </td>
                        <td>
                            <legend>Item Description</legend>
                            <input type='text' class='inputField' name='description' placeholder='Description' value='".(isset($record['M_DESC']) ? $record['M_DESC'] : null)."' required>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <legend>Price $</legend>
                            <input type='text' class='inputField' name='price' placeholder='Price per Unit' value='".(isset($record['M_PRICE']) ? number_format(($record['M_PRICE'] / 100), 2) : null)."' required>
                        </td>
                        <td>
                            <legend>Item Type</legend>
                            <select name='type' class='inputField' style='width:100%;' required>
                                <option value='' disabled hidden selected>Select a Type</option>
                                <option value='Accessories'>Accessories</option>
                                <option value='Clothing'>Clothing</option>
                                <option value='Flash Sheets'>Flash Sheets</option>
                                <option value='other'>Other</option>
                            </select>
                        </td>
                        </tr>
                    <tr>
                        <td colspan='2'>
                            <legend>Sizes</legend>
                            <input type='number' id='none' name='none' value='".(isset($sizes['NA']) ? $sizes['NA'] : 0)."' class='size-field' />
                            <label for='noSize'>No Size</label>
                            <input type='number' id='XS' name='xs' value='".(isset($sizes['XS']) ? $sizes['XS'] : 0)."' class='size-field' />
                            <label for='XS'>XS</label>
                            <input type='number' id='S' name='s' value='".(isset($sizes['S']) ? $sizes['S'] : 0)."' class='size-field' />
                            <label for='S'>S</label>
                            <input type='number' id='M' name='m' value='".(isset($sizes['M']) ? $sizes['M'] : 0)."' class='size-field' />
                            <label for='M'>M</label>
                            <input type='number' id='L' name='l' value='".(isset($sizes['L']) ? $sizes['L'] : 0)."' class='size-field' />
                            <label for='L'>L</label>
                            <input type='number' id='XL' name='xl' value='".(isset($sizes['XL']) ? $sizes['XL'] : 0)."' class='size-field' />
                            <label for='XL'>XL</label>
                            <input type='number' id='XXL' name='xxl' value='".(isset($sizes['XXL']) ? $sizes['XXL'] : 0)."' class='size-field' />
                            <label for='XXL'>XXL</label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan='2'>
                            <input type='file' name='image' id='image' onchange='changeLbl()'/>
                            <label for='image' id='fileUploader'>Add an Image...</label>
                            <a href='manageMerchandise.php'><input type='button' style='float:right;' class='delbutton' value='Back' /></a>
                            <input type='submit' name='save' value='Save' formaction='updateItem.php' class='button' style='float:right;' />
                        </td>
                    </tr>
                </form>
            </table> 
        </div>";
    }
    else
    {
        // User does not have access to this page. Redirect elsewhere
        header("location: invalid.php");
        die();
    }
?>