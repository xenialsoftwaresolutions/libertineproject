<html>
    <head>
        <link rel='stylesheet' href='management.css'>
        <link rel='stylesheet' href='modal.css'>
        <script src='modal.js'></script>
        <title>Libertine Tattoo - Orders</title>
    </head>
    <body>
        <div id='myModal' class='modal'>
            <!-- Modal content -->
            <div class='modal-content'>
                <div class='modal-header'>
                    <span type='button' class='close' onclick='return closeModal()'>&times;</span>
                    <h2>Libertine Tattoo</h2>
                </div>
                <div class='modal-body'>
                    <p id='modal-text'>Are you sure this order has been fulfilled? <br /><br /> WARNING: Marking this order as fulfilled will delete the record!</p>
                    <button type='button' class='modal-butt' id='noButt' onclick='closeModal()'>No</button>
                    <button type='button' class='modal-butt' id='yesButt' onclick='confirmClickForm("deleteOrder.php")'>Yes</button>
                </div>
            </div>
        </div>

        <nav>
            <ul id='ulNav'>
                <li id='liNav' style='float:left; background-color:#120fbf;'><a id='aNav' href = 'home.php' >Home</a></li>
                <li id='liNav' style='float:left; background-color:#120fbf; border-right:none;'><a id='aNav' href = 'manageWebsite.php' >Main Menu</a></li>
                <li id='liNav'><a id='aNav' href = 'managePortfolio.php' >My Portfolio</a></li>
                <li id='liNav'><a id='aNav' href = 'managePosts.php' >Blog</a></li>
                <li id='liNav'><a id='aNav' href = 'manageMerchandise.php' >Merchandise</a></li>
                <li id='liNav'><a id='aNav' href = 'manageOrders.php' >Orders</a></li>
                <li id='liNav' style='border-right:none;'><a id='aNav' href = 'manageEmployees.php' >Employees</a></li>
                <li id='liNav' style='float:right; background-color:#120fbf;'><a id='aNav' href='logout.php'>Logout</a></li>
            </ul>
        </nav>
        <?php
            // Chris Toth March 22, 2018

            session_start();

            require_once('connect.php');
            require_once('checkValidUser.php');
            require_once "sessionTimer.php";

            $accessArray = array("Artist", "Manager");

            sessionTimer();

            if (checkValidUser($_SESSION['accessLVL'], $accessArray, ""))
            {
                $conn = connect(); // Connect to database
                
                // query databases to populate the forms with orders
                //
                // select O_ID, O_PLACE_DATE, O_COST from order
                // select C_NAME from customer
                $sql = $conn->query("SELECT ORDERS.O_ID, ORDERS.O_PLACE_DATE, ORDERS.O_COST, CUSTOMER.C_NAME, ORDERS.C_ID FROM ORDERS INNER JOIN CUSTOMER ON ORDERS.C_ID = CUSTOMER.C_ID WHERE O_FULLFILL_DATE IS NULL;");
                
                if ($conn->error)
                {
                    echo $conn->error;
                    $conn->close();
                    die();
                }

                echo "
                <div align ='center' id='wrapper'>
                    <h1>Manage Orders</h1>
                    <table>
                        <tr>
                            <th>Order ID</th>
                            <th>Customer Name</th>
                            <th>Date of Order</th>
                            <th>Order Cost</th>
                        </tr>";
                while ($row = mysqli_fetch_assoc($sql))
                {
                    echo "
                    <form action = '' method='post' enctype='multipart/form-data'>
                        <tr>
                            <td>
                                <input type='text' value='{$row['O_ID']}' name='oID' class='inputField' readonly>
                            </td>
                            <td>
                                <input type='text' value='{$row['C_NAME']}' name='cNAME' class='inputField' readonly>
                            </td>
                            <td>
                                <input type='text' value='{$row['O_PLACE_DATE']}' name='oPLACED' class='inputField' readonly>
                            </td>
                            <td>
                                <input type='text' value='$" . number_format(($row['O_COST'] / 100), 2). "' name='oCOST' class='inputField' readonly>
                            </td>
                            <td>
                                <input type='submit' value='View' name='view' class='button' formaction='viewOrder.php' />
                            </td>
                        </tr>
                    </form>
                    ";
                }
                echo "</table></div>";
            }
            else
            {
                // User does not have access to this page. Redirect elsewhere
                header("location: invalid.php");
                die();
            }
        ?>
    </body>
</html>