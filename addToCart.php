<?php
    require_once "connect.php";
    session_start();

    /*unset($_SESSION['cart']);
    unset($_SESSION['total']);
    die();*/

    if(isset($_GET['itemID'], $_GET['size']))
    {

        $response = "Failed!";
        if(!isset($_SESSION['cart']))
        {
            $_SESSION['cart'] = array();
        }
        $itemID = $_GET['itemID'];
        $size = $_GET['size'];

        $contains = false;
        $IDkey = null;
        foreach(array_keys($_SESSION['cart']) as $key)
        {   
            if($key == "".$itemID)
            {
                foreach(array_keys($_SESSION['cart'][$itemID]) as $sizeKey)
                {
                    if($sizeKey == "".$size)
                    {
                        $contains = true;
                        $IDkey = $key;
                        break;
                    }
                }
            }
        }
        $conn = connect();
        if(!$conn)
        {
            die("Failed to connect");
        }

        $stockCheckSQL = $conn->prepare("SELECT * FROM INVENTORY WHERE M_ID = ?;");
        $stockCheckSQL->bind_param("i", $itemID);
        $stockCheckSQL->execute();
        $stockResult = $stockCheckSQL->get_result();
        $stockCheckSQL->fetch();
        $stockCheckSQL->close();

        $stock = mysqli_fetch_assoc($stockResult)[$size];

        if(isset($_SESSION['cart'][$IDkey][$size]))
        {

            if($stock ==  $_SESSION['cart'][$IDkey][$size] || $stock <  $_SESSION['cart'][$IDkey][$size])
            {
                $response = "No more in stock!";
                $conn->close();
                echo $response;
                die();
            }
        }

        if ($IDkey == null)
        {
            $IDkey = "".$itemID;
        }

        if(!$contains)
        {
            $_SESSION['cart'][$IDkey][$size] = 1;
        }
        else
        {
            $_SESSION['cart'][$IDkey][$size] = ($_SESSION['cart'][$IDkey][$size] + 1);
        }

        $sql = $conn->prepare("SELECT M_NAME FROM MERCH_ITEM WHERE M_ID = ?");
        $sql->bind_param("i", $itemID);
        $sql->execute();
        $sql->bind_result($result);
        $sql->fetch();
        $sql->close();


        if($size != "NA")
        {
            $response = "Success! Added {$result} to cart. Now have {$_SESSION['cart'][$IDkey][$size]} of size {$size} in cart.";
        }
        else
        {
            $response = "Success! Added {$result} to cart. Now have {$_SESSION['cart'][$IDkey][$size]} in cart.";
        }

        $itemSQL = $conn->prepare("SELECT * FROM MERCH_ITEM WHERE M_ID = ?;");
        

        $subtotal = 0;
        $GST = 0;
        foreach(array_keys($_SESSION['cart']) as $merchID)
        {
            $itemSQL->bind_param("i", $merchID);
            $itemSQL->execute();
            $itemData = $itemSQL->get_result();
            $itemSQL->fetch();
                    
            while ($itemInfo = mysqli_fetch_assoc($itemData))
            {
                foreach(array_keys($_SESSION['cart'][$merchID]) as $size)
                {
                    $price = ($itemInfo['M_PRICE']  * $_SESSION['cart'][$merchID][$size]);
                    $subtotal += $price;
                    $GST += (($price / 100) * 5); // 5 represents 5%
                }
            }
        }
        $_SESSION['subtotal'] = $subtotal;
        $_SESSION['GST'] = $GST;
        $conn->close();
        
        // pass in $_SESSION['cart']
        function quantity($arr)
        {
            $total = 0;
            foreach ($arr as $single)
            {
                foreach($single as $num)
                {
                    $total += $num;
                }
            }
            return $total;
        }

        $togo = array($response, quantity($_SESSION['cart']));
        echo json_encode($togo);
    }
    else
    {
        header("location: store.php");
    }
?>