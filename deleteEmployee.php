<?php
print"<title> Libertine Tattoo - Delete Employee</title>";
    require_once ('connect.php');
    require_once "checkValidUser.php";
    require_once "sessionTimer.php";
    session_start();

    $reqAccArr = array("Artist", "Manager");

    sessionTimer();

    if(checkValidUser($_SESSION['accessLVL'], $reqAccArr, ""))
    {

        $sID = $_POST['sID'];

        $conn = connect();

        $one = 1;
        $deleteEmpSQL = $conn->prepare("UPDATE STAFF SET S_DELETED = ? WHERE S_ID = ?");
        $deleteEmpSQL->bind_param("ii", $one, $sID);
        $result = $deleteEmpSQL->execute();
        $deleteEmpSQL->close();

        //$result = $conn->query("UPDATE STAFF SET P_DELETED = 1 WHERE S_ID = '$sID'");

        if($result)
        {
            $conn->close();
            echo "<script>alert('Post Deleted Successfully');</script>";
            header("location: manageEmployees.php");
            die();
        }
        else
        {
            $conn->close();
            echo "<script>alert('Post Failed Deletion!');</script>";
            header("location: manageEmployees.php");
            die();
        }
    }
    else
    {
        // User does not have access to this page. Redirect elsewhere
        header("location: invalid.php");
        die();
    }
?>