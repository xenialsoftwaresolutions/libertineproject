<?php
// Chris Toth March 10, 2018
//
// the user is redirected to this page if they do not have permission to access a page they clicked.
// this script will display information letting the user know. then redirect them back to the manage website page after 5 seconds.
echo "
    <!doctype html>

    <head>
        <script src='jquery.min.js'></script>
        <link rel='stylesheet' href='invalid.css'>
        <link rel='stylesheet' href='management.css'>
        <title> Libertine Tattoo - Invalid</title>
    </head>
    <body>
        <div id='wrapper-small'> 
            <h1>You do not have access to that page</h1>
            <img src='images/crybaby.jpg' height='300px' width='100%' />
            <h1 style='font-size:20px;'>Redirecting to the main page in 5 seconds...</h1>
        </div>
    </body>  
    </html>
";

    header("refresh:5;url=manageWebsite.php");
?>