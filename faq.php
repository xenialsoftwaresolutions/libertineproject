<?php
require_once("printHTML.php");

echo"
<!doctype html>

    <head>
        <script src='jquery.min.js'></script>
        <script src='faq.js'></script>
        <link rel='stylesheet' href='style.css'>
        <link rel='stylesheet' href='faq.css'>
        <link rel='stylesheet' href='loader.css'>
        <script src='loadingscreen.js'></script>
        <title> Libertine Tattoo - FAQ</title>
        <link href='https://fonts.googleapis.com/css?family=Work+Sans' rel='stylesheet'>
       
    </head>

<body>";

    printLoader();
    printNav();
    echo"

    <div id = 'headercontain'>
        
            <div class='center'>
                Frequently Asked <br>Questions
            </div>
    </div>

            

        

<div id='allfaqs'>
    <div class='faq faq1'>
        <h2>How do I book an appointment or a consultation?</h2>
        
        
        <p>Consults may be arranged over the phone (587.425.4219), by walking in and occasionally through our Facebook age.
         The best way to reach your artist is to email them directly to book with them. Trying to book with Steve? Steve’s books are currently closed, 
         however he is willing to book in bigger projects and rebook regular clients until he is able to catch up with his busy schedule.
         Best way to get into contact with Steve is to come into the shop on Tuesday and Thursday Evenings.
        </p>
        
    </div>

    <div class='faq faq1'>
            <h2>Deposits? </h2>
                
                
    
        <p>All appointments require a deposit and no appointment can be made with out a deposit. DEPOSITS ARE TRANSFERBLE BUT NOT REFUNDABLE. 
            Deposits will depend on what you are getting tattooed and who is tattooing you,
             they can range from $80-200+. Your deposit will be put towards the final price of your tattoo and can be carried over for multiple session tattoos.
        </p>
                    
        
    
    </div>

    <div class='faq faq1'>
            <h2>Cancellation policy.</h2>
            
                
                <p>We understand you may live a busy life and unforeseen circumstances happen. Please provide us with 48 hours notice for any cancellations or rescheduling of appointments. 
                Rescheduling or cancelling your appointments without 48 hours notice will forfeit your deposit.
                 No shows or showing up more than 20 minutes late for your appointment will also be taken as forfeiting your deposit.
                 Please respect our time and our other clients time. If something comes up before your appointment, please call the shop.</p>
            
    </div>

    <div class='faq faq1'>
            <h2>Payments.</h2>
            
                
                <p>We are CASH ONLY. Kindly, we do have an ATM located in our shop. Please contact your artist to request any other payment arrangements. </p>
            
    </div>

    <div class='faq faq1'>
             <h2>Walk-Ins?</h2>
        
            
            <p>Yes! We will happily take any walk-ins pending availability of the artists.
                     Feel free to give the shop a call first and we will gladly let you know if any of our artists have time. Check out our blogs for any up coming guest artists as well!
                <br>
                     Also, TATTUESDAY! Every Tuesday all of our full time artists clear their schedules for walk-ins. Open 10-late! 
                     This helps us open up our books and shorten the wait time for bigger custom projects. Little tat bangers welcome!
            </p>
        
    </div>

    <div class='faq faq1'>
            <h2>Pricing.</h2>
    
        
            <p>All of our Tattooers charge at their own discretion and this can be determined at your consultation. Our pricing reflects the size, placement, and detail of your tattoo, 
            as well as the time and effort put into your tattoo. 
            Typically, our shop minimum is $100 with Kelsey and Steve charging $160 per hour and Craig charging by the piece.  </p>
    
    </div>

    <div class='faq faq1'>
            <h2>Piercings?</h2>
    
        
            <p>WE DO NOT DO ANY PIERCINGS! TATTOOS ONLY!  </p>
    
    </div>

        
</div>

    
         <img class='shopphoto' src='images/setup.jpg' height='84%' width='15%'></img>
   

         ";
         printFooter();
        echo"
         
      <button onclick='topFunction()' id='myBtn' title='Go to top'>Top</button>
        
  
      <script>

          window.onscroll = function() {scrollFunction()};

          function scrollFunction() {
              if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                  document.getElementById('myBtn').style.display = 'block';
              } else {
                  document.getElementById('myBtn').style.display = 'none';
              }
          }

          function topFunction() {
              document.body.scrollTop = 0;
              document.documentElement.scrollTop = 0;
          }

      </script>
        

</body>
"/*<div id='footer2'>
    <div class = 'homepage'><a class = 'whitetext' href='home.php'>FAQ</a></div>
    <div class = 'homepage'><a class = 'whitetext' href='home.php'>Shop</a></div>
    <div class = 'homepage'><a class = 'whitetext' href='home.php'>Artist</a></div>
    <div class = 'homepage'><a class = 'whitetext' href='home.php'>Blog</a></div>
    <div class = 'homepage'><a class = 'whitetext' href='home.php'>Home</a></div>
</div>"*/



;

?>