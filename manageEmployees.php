<html>
    <head> 
            <link rel='stylesheet' href='management.css'>
            <link rel='stylesheet' href='manageEmployees.css'>
            <link rel='stylesheet' href='modal.css'>
            <script src='modal.js'></script>
            <title> Libertine Tattoo - Manage Employees</title>
    </head>
    <body>
        <div id='myModal' class='modal'>
            <!-- Modal content -->
            <div class='modal-content'>
                <div class='modal-header'>
                    <span type='button' class='close' onclick='return closeModal()'>&times;</span>
                    <h2>Libertine Tattoo</h2>
                </div>
                <div class='modal-body'>
                    <p id='modal-text'>Are you sure you want to delete this Employee Record?</p>
                    <button type='button' class='modal-butt' id='noButt' onclick='closeModal()'>No</button>
                    <button type='button' class='modal-butt' id='yesButt' onclick='confirmClickForm("deleteEmployee.php")'>Yes</button>
                </div>
            </div>
        </div>

        <?php
            /********************************************************************************************************************************************
             *                                              php written by: Michael Barfuss   Date: March 10, 2018                                      *
             *                                             HTML written by:_________________  Date: _______________                                     *
             ********************************************************************************************************************************************/

            require_once ('checkValidUser.php');
            require_once ('connect.php');
            require_once "sessionTimer.php";
            session_start();

            $reqAccArr = array("Manager");

            sessionTimer();

            if(checkValidUser($_SESSION['accessLVL'], $reqAccArr, "")) // checkValidUser returns bool
            {


                $sID = $_SESSION['sID']; // ID retrieved at login

                $conn = connect(); // Connect to database

                echo "

                <nav>
                    <ul id='ulNav'>
                        <li id='liNav' style='float:left; background-color:#120fbf;'><a id='aNav' href = 'home.php' >Home</a></li>
                        <li id='liNav' style='float:left; background-color:#120fbf; border-right:none;'><a id='aNav' href = 'manageWebsite.php' >Main Menu</a></li>
                        <li id='liNav'><a id='aNav' href = 'managePortfolio.php' >My Portfolio</a></li>
                        <li id='liNav'><a id='aNav' href = 'managePosts.php' >Blog</a></li>
                        <li id='liNav'><a id='aNav' href = 'manageMerchandise.php' >Merchandise</a></li>
                        <li id='liNav'><a id='aNav' href = 'manageOrders.php' >Orders</a></li>
                        <li id='liNav' style='border-right:none;'><a id='aNav' href = 'manageEmployees.php' >Employees</a></li>
                        <li id='liNav' style='float:right; background-color:#120fbf;'><a id='aNav' href='logout.php'>Logout</a></li>
                    </ul>
                </nav>";

                $employees = $conn->query("SELECT S_ID, S_NAME, S_POSITION FROM STAFF WHERE S_DELETED = 0");

                /*****************************************************************
                 *           Populates page with non-deleted Employees           *
                 *****************************************************************/
                echo"
                <div id='wrapper' align ='center'>
                <h1> Manage Employees</h1>
                <table style='width:60%'>
                <tr>
                    <th>Name</th>
                    <th> ID</th>
                    <th> Postion</th>
                    <th colspan='2'> Action </th>
                </tr>";
                while ($row = mysqli_fetch_assoc($employees))
                {
                echo "
                <div align ='center' class='separate'>
                    <form action = '' method='post' enctype='multipart/form-data' class='formMargin'>
                        <table>
                            <tr>
                                <td>
                                    <input type='text' value='manageEmployees' name='parent' hidden readonly>
                                    <input type = 'text' name = 'firstVisit' value = 'true' hidden readonly >
                                </td>
                                <td>
                                    <input type='text' value='".$row['S_NAME']."' name='name' class='inputField' readonly>
                                </td>
                                <td>
                                    <input type='text' value='".$row['S_ID']."' name='sID' class='inputField' readonly>
                                </td>
                                <td>
                                    <input type='text' value='".$row['S_POSITION']."' name='position' class='inputField' readonly>
                                </td>
                                <td>
                                    <input type='submit' value='Update' name='update' class='button' formaction='updateEmployee.php'>
                                </td>
                                <td>   
                                    <input type='button' value='Delete' name='delete' class='delbutton' onclick='displayModalForm(this.form)' />
                                </td>
                            </tr>
                        </table>
                    </form>
                ";
                }
                echo " </table></div>
                <form action = 'createEmployee.php' method='post' enctype='multipart/form-data' />
                    <input type='text' value='manageEmployees.php' name='firstVisit' hidden readonly />
                    <input type='submit' value='Add New Employee' id='createButt'>
                </form>
                ";// floating create post button
                
                
                $conn->close();
            }
            else
            {
                // User does not have access to this page. Redirect elsewhere
                header("location: invalid.php");
                die();
            }
        ?>
    </body>
</html>
