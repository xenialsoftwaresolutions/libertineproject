<?php
    require_once ('connect.php');
    require_once "checkValidUser.php";
    require_once "sessionTimer.php";
    session_start();

    $reqAccArr = array("Artist", "Manager");

    sessionTimer();

    function getDatetimeNow()   // returns the date and time in the format SQL accepts
    {

    $tz_object = new DateTimeZone('America/Edmonton');
    //date_default_timezone_set('America/Edmonton'); // alternate method

    $datetime = new DateTime();
    $datetime->setTimezone($tz_object);
    return $datetime->format('Y\-m\-d');
    }

    if(checkValidUser($_SESSION['accessLVL'], $reqAccArr, ""))
    {
        $conn = connect();
        
        // get neccessary variables
        $oID = $_POST['oID'];
        $current_time = GetDatetimeNow();

        // update order table to mark an order as complete
        $sql = $conn->query("UPDATE ORDERS SET O_FULLFILL_DATE='{$current_time}' WHERE O_ID='{$oID}';");

        if($conn->error)
        {
            die($conn->error . "\npost UPDATE Failed. ln 80");
        }
        else
        {
            $conn->close();
            header("location: manageOrders.php");
            die();
        }
    }
    else
    {
        // User does not have access to this page. Redirect elsewhere
        header("location: invalid.php");
        die();
    }

?>
