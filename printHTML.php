<?php

function printNav(...$args)
{
    echo" <!-- Nav bar modified from https://www.w3schools.com/css/css_navbar.asp -->
        <nav>
            <ul id='ulNav'>
                <img id = 'banner-logo' src='images/banner.png'></img>
                <li id='liNav'><a id='aNav' href = 'home.php' class = 'highlight'>HOME</a></li>
                <li id='liNav'><a id='aNav' href = 'blog.php' class = 'highlight'>BLOG</a></li>
                <li id='liNav'><a id='aNav' href = 'artists.php' class = 'highlight'>ARTISTS</a></li>
                <li id='liNav'><a id='aNav' href = 'faq.php' class = 'highlight'>FAQ</a></li>";
                foreach ($args as $item)
                {
                    echo $item;
                }
                echo"
            </ul>
        </nav>";
}



function printFooter()
{
    echo"

    <div id='footer'>
        <div class= 'breaker'></div>
            <div id='informationcontainer'>
                <img class='contacts' src='images/location.png' height='13%' width='42%'></img>
                <img class='contacts' src='images/phone.png' height='13%' width='40%'></img>
                <img class='contacts' src='images/mail.png' height='13%' width='40%'></img>
                <br />
                <a href='login.php'><button id='loginButt'>Employee Login</button></a>
            </div>
            

            <div id='ulforinformation'>
                <ul>
                    <li>1402 17 St S, Lethbridge</li>
                    <li>(587) 425-4219</li>
                    <li><a href ='artists.php' class = 'whitetext'>Contact an Artist Directly</a></li>
                </ul>
            </div>

            <img class='logo' src='images/logo.png' height='75%' width='15%' style='padding-top:1%;'></img>

            <a href='https://www.facebook.com/libertinetattoo/' style='text-decoration:none;'>
                <img id = 'facebook' src='images/facebook.png'></img>
            </a>

            <a href='https://www.instagram.com/libertinetattoo/?hl=en' style='text-decoration:none;'>
                <img id = 'instagram' src='images/instagram.png'></img>
            </a>

            <a href='aboutTeam.php'>
            <img id = 'instagram' src='images/xenial.png'></img>
        </a>
    </div>";
}

function printLoader() 
{
    echo "
        <div class='loadingScreen'>
            <div class='loader-banner'>
                <div class='banner'>
                    <b>Libertine Tattoo</b>
                    <div class='banner-left'></div>
                    <div class='banner-right'></div>
                </div>
            </div>
        </div>";
}
?>