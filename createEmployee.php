<div id='myModal' class='modal'>
    <div class='modal-content'>
        <div class='modal-header'>
            <span type='button' class='close' onclick='closeModal()'>&times;</span>
            <h2>Libertine Tattoo</h2>
        </div>
        <div class='modal-body'>
            <p id='modal-text'>Successfully created new employee! Would you like to create another?</p>
            <button type='button' class='modal-butt' id='noButt' onclick='noClickReturn("createEmployee.php")'>No</button>
            <button type='button' class='modal-butt' id='yesButt' onclick='closeAndSubmit("createEmployee.php")'>Yes</button>
        </div>
    </div>
</div>
<script>
    function disable(obj)
    {
        obj.disabled = true;
        setTimeout(function(){obj.disabled = false;},10000);
    }
    </script>
<?php
    /********************************************************************************************************************************************
    *                                              php written by: Michael Barfuss   Date: March 12, 2018                                      *
    *                                             HTML written by:_________________  Date: _______________                                     *
    ********************************************************************************************************************************************/

    require ('checkValidUser.php'); // contains function to validate user (returns bool)
    require ('connect.php');        // Contains function to connect to database (returns mysqli connection object)
    require ('salt.php');
    require_once "sessionTimer.php";
    session_start();

    sessionTimer();

    $reqAccArr = array("Artist", "Manager");  // array containing required staff positions to access this page

    if(checkValidUser($_SESSION['accessLVL'], $reqAccArr, '')) // checkValidUser returns bool
    {
        
        $name = $parent = $firstVisit = $sID = $position = '';

        $sID = $_SESSION['sID']; // ID retrieved at login

        $conn = connect();
        $fieldsFilled = false;


/****************    Evaluates whether required fields are filled in    *******************/
                                                                                        ////
        if(isset($_POST['name'], $_POST['position'], $_POST['username'],                ////
         $_POST['password'], $_FILES['image']))                                         ////
        {                                                                               ////
            $fieldsFilled = true;                                                       ////
        }else                                                                           ////
        {                                                                               ////
            $fieldsFilled = false;                                                      ////
        }                                                                               ////
/******************************************************************************************/

        if ($fieldsFilled)
        {	

            // formats image for database
            $image = file_get_contents($_FILES['image']['tmp_name']);
            //$image = mysqli_real_escape_string($conn, $image);
            $extension = strtolower(substr($_FILES['image']['name'], strrpos($_FILES['image']['name'], '.') + 1));
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mime = $_FILES['image']['type'];
            finfo_close($finfo);

/*****************    Makes sure file is an image of proper type    *****************/
                                                                                    //
            $extArr = array('png', 'jpg','jpeg','gif');                             //
            if(!in_array($extension, $extArr))                                      //
            {                                                                       //
                echo "<script>"                                                     //
                ."alert('File Format Must Be png, jpeg/jpg, or gif');"              //
                ."</script>";                                                       //
                header("location: updatePost.php");                                 //
                die();                                                              //
            }                                                                       //
/************************************************************************************/
                                                                    
/*******************************    Insert Employee into the database    ************************************************/
            
            // Hashing passwords using the sha256 algorithm
            // third arguement False ouputs data as lowercase hexits
            $pword = $_POST['password'];             // Get password
            $salt = generateSalt();                  // Get Salt
            $pword = $pword . $salt;                 // Combine password to salt
            $pword = hash("sha256", $pword, false);  // Hash the salted password
            // the salt and the hashed salted password are entered into the database.

            $S_DELETED = 0;                                                                                             //
            $null = NULL;                                                                                               //
            $sql = $conn->prepare("INSERT INTO STAFF (S_NAME, S_POSITION, S_PWORD, S_UNAME, S_DELETED,"                 //
            ."S_IMAGE, S_MIME, S_EMAIL, S_PHONE, S_SALT) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");                      //
            $sql->bind_param("ssssibssss", $_POST['name'], $_POST['position'], $pword, $_POST['username']                //
            , $S_DELETED, $null, $mime, $_POST['email'], $_POST['phone'], $salt);                                       //
            $sql->send_long_data(5, $image);                                                                            //
            $result = $sql->execute();                                                                                  //
            $sql->get_result();
                                                                                                                        //
                                                                                                                        //
            /*$result = $conn->query("INSERT INTO STAFF (S_NAME, S_POSITION, S_PWORD, S_UNAME, S_DELETED,"              // 
            ."S_IMAGE, S_MIME, S_EMAIL, S_PHONE)"                                                                       //
            ."VALUES ('{$_POST['name']}', '{$_POST['position']}', '{$_POST['password']}', '{$_POST['username']}',"      //
            ."0, '$image', '$mime', '{$_POST['email']}', '{$_POST['phone']}')");*/                                      //
            if(!$result)                                                                                                //
            {                                                                                                           //
                echo "<script>"                                                                                         //
                    ."alert('Failed To Create: result = {$result} ERR: {$sql->errno} ');" // Notify user that employee creation failed                          //
                ."</script>";                                                                                           //
                die("{$sql->error}");
            }                                                                                                           //
            $sql->close();
                                                                                                                        //
            $uname = $_POST['username'];                                                                                //
            $sql = $conn->prepare("SELECT S_ID FROM STAFF WHERE S_UNAME = ? AND S_PWORD = ?");                          //
            $sql->bind_param("ss", $uname, $pword);                                                                     //
            $sql->execute();                                                                                            //
            $sql->bind_result($recievedSID);                                                                            //
            $sql->fetch();                                                                                              //
            $sql->close();                                                                                              //
                                                                                                                        //
            $desc = $_POST['bio'];                                                                                      //
            $sql = $conn->prepare("INSERT INTO PORTFOLIO (S_ID, PO_DESC) VALUES (?, ?)");                               //
            $sql->bind_param("is", $recievedSID, $desc);                                                                //
            $result = $sql->execute();                                                                                  //
                                                                                                                        //
            /*$recievedSID = mysqli_fetch_assoc($conn->query("SELECT S_ID FROM STAFF WHERE "                            //
            ."S_UNAME = '{$_POST['username']}'AND S_PWORD = '{$_POST['password']}'"))['S_ID'];                          //
            $result = $conn->query("INSERT INTO PORTFOLIO (S_ID, PO_DESC) VALUES ('$recievedSID', '{$_POST['bio']}')"); */
            if(!$result)                                                                                                //
            {                                                                                                           //
                // Notify user that employee creation failed                                                            //
                echo "                                                                                                  
                <script>
                    displayModal();
                    setText('The employee was not created.<p>Would you like to try again?</p>');
                </script>";                                                                                             //
            }                                                                                                           //
/************************************************************************************************************************/
            else
            {
                echo "
                <script>
                    displayModal();
                    setText('Successfully created a new employee! </p>');
                </script>";
            }

            if($_POST['continueCreating'] == 'false')
            {
                header("location: manageEmployees.php");
                die();
            }

        }else if(isset($_POST['firstVisit']))
        {
            //do nothing
        }
        else
        {
            // Echo error: fields must be filled
            echo "
            <script>
                displayModal();
                setText('All fields must be Filled! <p>Would you like to try again?{$_POST['firstVisit']}</p>');
            </script>"; 
        }

        

        $conn->close(); // Close the connection to the database


        print"
        <!doctype>
        <head>
            <link rel='stylesheet' href='management.css'>
            <link rel='stylesheet' href='createEmployee.css'>
            <link rel='stylesheet' href='modal.css'>
            <script src='changeLbl.js'></script>
            <script src='modal.js'></script>
            <title>Libertine Tattoo - Create Employee</title>
        </head>
        <body>
        
        <nav>
            <ul id='ulNav'>
                <li id='liNav' style='float:left; background-color:#120fbf;'><a id='aNav' href = 'home.php' >Home</a></li>
                <li id='liNav' style='float:left; background-color:#120fbf; border-right:none;'><a id='aNav' href = 'manageWebsite.php' >Main Menu</a></li>
                <li id='liNav'><a id='aNav' href = 'managePortfolio.php' >My Portfolio</a></li>
                <li id='liNav'><a id='aNav' href = 'managePosts.php' >Blog</a></li>
                <li id='liNav'><a id='aNav' href = 'manageMerchandise.php' >Merchandise</a></li>
                <li id='liNav'><a id='aNav' href = 'manageOrders.php' >Orders</a></li>
                <li id='liNav' style='border-right:none;'><a id='aNav' href = 'manageEmployees.php' >Employees</a></li>
                <li id='liNav' style='float:right; background-color:#120fbf;'><a id='aNav' href='logout.php'>Logout</a></li>
            </ul>
        </nav>

        <div align='center' id='wrapper'>
            <h1> New Employee</h1>
            <form action = 'createEmployee.php' method='post' enctype='multipart/form-data' name='create'>
                <input type='text' name='continueCreating' id='continueCreating' value='true' hidden readonly >
                <table>
                    <tr>
                        <td>
                            <input type='text' class='inputField' name='name' placeholder='John Doe (First and last name)' required>
                        </td>
                        <td>
                            <input type='text' class='inputField' name='username' placeholder='Username' required>
                        </td>
                        <td>
                            <input type='password' class='inputField' name='password' placeholder='Password' required>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type='email' class='inputField' name='email' placeholder ='Email' required>
                        </td>
                        <td>
                            <input type='tel' class='inputField' name='phone' placeholder='Phonenumber' required>
                        </td>
                        <td>
                            <select name='position' class='inputField' style='width:100%;' required>
                                <option disabled hidden selected>Select a Position</option>
                                <option value='Artist'>Artist</option>
                                <option value='Manager'>Manager</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                            <td colspan='3'>
                                <textarea name='bio' value='".(isset($bio) ? $bio : null)."' maxlenth='1000' style='width:100%; height:100px; resize:none;' placeholder='Tell us a little bit about yourself' required>".(isset($bio) ? $bio : null)."</textarea>
                            </td>
                        </tr>
                </table>

                <input type='file' name='image' id='image' onchange='changeLbl()' required/>
                <label for='image' id='fileUploader'>Choose an Image...</label>
                <input class='button' type='button' value='Create New Employee' name='create new employee' onclick='displayModalForm(this.form)'>
                <a href='manageEmployees.php'><input type='button' class='delbutton' value='Back' /></a>
            
            </form>
        </div>
        </body>
        </html>";
    }
    else
    {
        // User does not have access to this page. Redirect elsewhere
        header("location: invalid.php");
        die();
    }
?>