<form method='POST'>
    <input type='text' name='pass' />
    <input type='submit' name='submit' />
</form>
<?php

require ('../salt.php');

if (isset($_REQUEST['pass']))
{
    $pass = $_REQUEST['pass'];
    
    $salt = generateSalt();
    $pass = $pass . $salt;

    $pass = hash('sha256', $pass, false);

    echo "This is the your salt <b>" . $salt . "</b><br /><br />"; 
    echo "This is your password <b>" . $pass . "</b>";
}

?>