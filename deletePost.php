<?php
    print"<title> Libertine Tattoo - Delete Post</title>";
    require_once ('connect.php');
    require_once "checkValidUser.php";
    require_once "sessionTimer.php";
    session_start();

    $reqAccArr = array("Artist", "Manager");

    sessionTimer();

    if(checkValidUser($_SESSION['accessLVL'], $reqAccArr, ""))
    {

        $postID = $_POST['pID'];

        $conn = connect();

        $one = 1;
        $staffID = $_SESSION['sID'];
        $deletePostSQL = $conn->prepare("UPDATE POST SET P_DELETED = ? WHERE S_ID = ? AND P_ID = ?");
        $deletePostSQL->bind_param("iii", $one, $staffID, $postID);
        $result = $deletePostSQL->execute();
        $deletePostSQL->close();

        //$result = $conn->query("UPDATE POST SET P_DELETED = 1 WHERE P_ID = '$postID'");

        if($result)
        {
            $conn->close();
            echo "<script>alert('Post Deleted Successfully');</script>";
            if($_REQUEST['parent'] === "managePortfolio.php")
            {
                header("location: managePortfolio.php");
                die();
            }
            else
            {
                header("location: managePosts.php");
                die();
            }
        }
        else
        {
            $conn->close();
            echo "<script>alert('Post Failed Deletion!');</script>";
            header("location: {$_REQUEST['parent']}");
            die();
        }
    }
    else
    {
        // User does not have access to this page. Redirect elsewhere
        header("location: invalid.php");
        die();
    }
?>