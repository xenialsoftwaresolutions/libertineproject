<?php

    //Import PHPMailer classes into the global namespace
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;

    require_once('printHTML.php');
    require_once('vendor/autoload.php');
    require_once('./config.php');
    require_once('connect.php');

    print"
    <!doctype>
    <head>
        <link rel='stylesheet' href='style.css'>
        <link rel='stylesheet' href='charge.css'>
        <title>Libertine Tattoo</title>
    </head>
    <body>";
    printNav();


    /*if(empty($_SERVER["HTTPS"]) || $_SERVER["HTTPS"] !== "on")
    {
        header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
        exit();
    }*/

    /******************************************************************
    * Code found on stack overflow to get proper date and time format *
    *******************************************************************/
    function getDatetimeNow()   // returns the date and time in the format SQL accepts
    {

        $tz_object = new DateTimeZone('America/Edmonton');
        //date_default_timezone_set('America/Edmonton'); // alternate method

        $datetime = new DateTime();
        $datetime->setTimezone($tz_object);
        return $datetime->format('Y\-m\-d\ h:i:s');
    }

    try 
    {
        session_start();


        $returningCustomer = false;
        $conn = connect();
        $result = $conn->query("SELECT * FROM CUSTOMER");
        while($row = mysqli_fetch_assoc($result))
        {
            // check if customer info in database
            if($row['C_ADDRESS'] == $_POST['line1'] && $row['C_CITY'] == $_POST['city'] && $row['C_PROVINCE'] == $_POST['province']
            && $row['C_POSTAL_CODE'] == $_POST['postalCode'] && $row['C_NAME'] == $_POST['custName'] && $row['C_EMAIL'] == $_POST['email'])
            {
                $returningCustomer = true;
            }
        }

        $address = $_POST['line1'];
        $city = $_POST['city'];
        $province = $_POST['province'];
        $postal = $_POST['postalCode'];
        $name = $_POST['custName'];
        $email = $_POST['email'];
        $price = ($_SESSION['subtotal'] + $_SESSION['GST']);


        if(!$returningCustomer)
        {
            // insert into customer (shipping info)
            $newCust = $conn->prepare("INSERT INTO CUSTOMER (C_ADDRESS, C_CITY, C_PROVINCE, C_POSTAL_CODE, C_NAME, C_EMAIL)
            VALUES (?, ?, ?, ?, ?, ?);");
            $newCust->bind_param("ssssss", $address, $city, $province, $postal, $name, $email);
            $success = $newCust->execute();
            if(!$success)
            {
                die("Failed to insert new customer");
            }
            $newCust->fetch();
            $newCust->close();
        }

        $currTime = getDatetimeNow();

        $cID_sql = $conn->prepare("SELECT C_ID FROM CUSTOMER WHERE C_ADDRESS =  ? AND C_CITY = ? AND C_PROVINCE = ? AND C_POSTAL_CODE = ? AND C_NAME = ? AND C_EMAIL = ?;");
        $cID_sql->bind_param("ssssss", $address, $city, $province, $postal, $name, $email);
        $cID_sql->execute();
        $cID_sql->bind_result($cID);
        $cID_sql->fetch();
        $cID_sql->close();

        $NULL = NULL;
        // insert into orders (customer ID, placement date, price)
        $order = $conn->prepare("INSERT INTO ORDERS (C_ID, O_PLACE_DATE, O_FULLFILL_DATE, O_COST) VALUES (?, ?, ?, ?);");
        $order->bind_param("issi", $cID, $currTime, $NULL, $price);
        $success = $order->execute();
        if(!$success)
        {
            die("Failed to create order");
        }

        $oID = mysqli_fetch_assoc($conn->query("SELECT MAX(O_ID) FROM ORDERS"))['MAX(O_ID)'];

        $orderItemSQL = $conn->prepare("INSERT INTO MERCH_ORDER (O_ID, M_ID, SIZES, QUANTITY) VALUES (?, ?, ?, ?);");
        $itemUpdateSQL = $conn->prepare("UPDATE INVENTORY SET NA = ?, XS = ?, S = ?, M = ?, L = ?, XL = ?, XXL = ?  WHERE M_ID = ?");
        $selectCurrentAmount = $conn->prepare("SELECT * FROM INVENTORY WHERE M_ID = ?");
        $subtotal = 0;


        $NAremaining = NULL;
        $XSremaining = NULL;
        $Sremaining = NULL;
        $Mremaining = NULL;
        $Lremaining = NULL;
        $XLremaining = NULL;
        $XXLremaining = NULL;

        // foreach session cart: insert into merch_order O_ID, M_ID
        foreach (array_keys($_SESSION['cart']) as $mID)
        {


        

            $amountRemaining;
            foreach(array_keys($_SESSION['cart'][$mID]) as $size)
            {
                //echo($size);
                //die();

                $selectCurrentAmount->bind_param("i", $mID);
                $selectCurrentAmount->execute();
                $amountResult = $selectCurrentAmount->get_result();
                $selectCurrentAmount->fetch();

                $amountArr = mysqli_fetch_assoc($amountResult);
                $amount = $amountArr[$size];

                $orderAmount = $_SESSION['cart'][$mID][$size];

                
                $orderItemSQL->bind_param("iisi", $oID, $mID, $size, $orderAmount);
                $orderItemSQL->execute();
                $orderItemSQL->fetch();

                $amountRemaining = $amount - $_SESSION['cart'][$mID][$size];

                if($amountRemaining < 0)
                {
                    unset($_SESSION['cart'], $_SESSION['subtotal'], $_SESSION['GST']);

                    $outputMSG = "<h2 class='title-msg'>OUT OF STOCK<h2><h2> Your card has not been charged<h2>";

                    header("Refresh: 5; URL=home.php");
                    die();
                }
                
                if($size == "NA")
                {
                    $NAremaining = $amountRemaining;
                }
                else if($size == "XS")
                {
                    $XSremaining = $amountRemaining;
                }
                else if($size == "S")
                {
                    $Sremaining = $amountRemaining;
                }
                else if($size == "M")
                {
                    $Mremaining = $amountRemaining;
                }
                else if($size == "L")
                {
                    $Lremaining = $amountRemaining;
                }
                else if($size == "XL")
                {
                    $XLremaining = $amountRemaining;
                }
                else if($size == "XXL")
                {
                    $XXLremaining = $amountRemaining;
                }
            }
            

            if ($NAremaining === NULL)
            {
                $NAremaining = $amountArr["NA"];
            }
            if($XSremaining === NULL)
            {
                $XSremaining = $amountArr["XS"];
            }
            if($Sremaining === NULL)
            {
                $Sremaining = $amountArr["S"];
            }
            if($Mremaining === NULL)
            {
                $Mremaining = $amountArr["M"];
            }
            if($Lremaining === NULL)
            {
                $Lremaining = $amountArr["L"];
            }
            if($XLremaining === NULL)
            {
                $XLremaining = $amountArr["XL"];
            }
            if($XXLremaining === NULL)
            {
                $XXLremaining = $amountArr["XXL"];
            }

            $itemUpdateSQL->bind_param("iiiiiiii", $NAremaining, $XSremaining, $Sremaining, $Mremaining, $Lremaining, $XLremaining, $XXLremaining, $mID);
            $itemUpdateSQL->execute();
            $itemUpdateSQL->fetch();
        }

        $NAremaining;
        $XSremaining;
        $Sremaining;
        $Mremaining;
        $Lremaining;
        $XLremaining;
        $XXLremaining;

        $orderItemSQL->close();
        $itemUpdateSQL->close();

    
        //////////////////////////////////////////////////////////   Charge the card   //////////////////////////////////////////////////////////////
        $charge = \Stripe\Charge::create(array(
            "amount" => ($_SESSION['subtotal'] + $_SESSION['GST']),
            "currency" => "cad",
            "card" => $_POST['stripeToken'],
            "description" => "Charge for Libertine Merchandise.",
            "receipt_email" => $_POST['email']
        ));
        //send the file, this line will be reached if no error was thrown above
        $outputMSG = "<h2 class='title-msg'>Thank you! Your order is being processed.<h2>\n <h2>You will recieve an email shortly with your invoice and order information.</h1>";
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        unset($_SESSION['cart'], $_SESSION['subtotal'], $_SESSION['GST']);
        header("Refresh: 5; URL=store.php");
        
    
        //you can send the file to this email:
        //echo $_POST['stripeEmail'];
    }

        //catch the errors in any way you like
        
    catch(Stripe_CardError $e) 
    {
        echo "Stripe says: Card Error";
        header("Refresh: 5; URL=store.php");
        die();
    }
        
    catch (Stripe_InvalidRequestError $e)
    {
        // Invalid parameters were supplied to Stripe's API
        echo "Stripe says: Invalid Parameters";
        header("Refresh: 5; URL=store.php");
        die();
    } 
        
    catch (Stripe_AuthenticationError $e) 
    {
        // Authentication with Stripe's API failed
        // (maybe you changed API keys recently)
        echo "Stripe says: Authentication Failed";
        header("Refresh: 5; URL=store.php");
        die();
    } 
        
    catch (Stripe_ApiConnectionError $e) 
    {
        // Network communication with Stripe failed
        echo "Stripe says: Network Error";
        header("Refresh: 5; URL=store.php");
        die();
    } 
        
    catch (Stripe_Error $e) 
    {
        echo "Stripe says: Something went wrong";
        header("Refresh: 5; URL=store.php");
        die();
        // Display a very generic error to the user, and maybe send
        // yourself an email
    } 
        
    catch (Exception $e) 
    {
        echo "Sorry, something went wrong...";
        header("Refresh: 5; URL=store.php");
        die();
        // Something else happened, completely unrelated to Stripe
    }
    finally
    {
        echo "
        <div class='container'>
            {$outputMSG}
            <img src='images/crybaby.jpg' id='image-checkout'/>
        </div>
        ";

        printFooter();
    }
    /*ini_set("SMTP", "smtp.gmail.com");
        ini_set("smtp_port", "587");
        ini_set("sendmail_from", "mikeyb.2109@gmail.com");

        $message = "The mail message was sent with the following mail setting:\r\nSMTP = smtp.gmail.com\r\nsmtp_port = 25\r\nsendmail_from = mikeyb.2109@gmail.com";

        $headers = "From: mikeyb.2109@gmail.com";


        mail("{$_POST['email']}", "Testing", $message, $headers);*/


        /*
        // the message
        $msg = "DO NOT REPLY.\nThis is an automated message from an unregulated address";

        // use wordwrap() if lines are longer than 70 characters
        $msg = wordwrap($msg,70);

        $headers = "From: mikeyb.2109@gmail.com" . "\r\n" .
                    "CC: {$_POST['email']}";

        ini_set("SMTP", "smtp.gmail.com");
        ini_set("smtp_port", "465");
        ini_set("sendmail_from", "mikeyb.2109@gmail.com");
        // send email
        mail("{$_POST['email']}","My subject",$msg, $headers);

        /*$mail = new PHPMailer(true);

        $mail->isSMTP();
        $mail->Host = "ssl://smtp.gmail.com";  //gmail SMTP server
        $mail->SMTPAuth = true;
        $mail->Username = 'mikeyb.2109@gmail.com';   //username
        $mail->Password = 'Cardst0ncougars1';   //password
        $mail->SMTPSecure = 'ssl';
        $mail->Port = 465;                    //SMTP

        $mail->setFrom('mikeyb.2109@gmail.com', 'Mikey!');
        $mail->addAddress("{$_POST['email']}", "{$_POST['custName']}");
        
        $mail->isHTML(true);
        
        $mail->Subject = 'Email subject';
        $mail->WordWrap = 50;
        $mail->Body    = 'Email Body';
        
        if (!$mail->send())
        {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } 
        else
        {
            echo 'Message has been sent';
        }*/
?>