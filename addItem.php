<html>
    <head>
        <link rel='stylesheet' href='management.css'>
        <link rel='stylesheet' href='modal.css'>
        <link rel='stylesheet' href='updateItem.css'>
        <script src='modal.js'></script>
        <script src='changeLbl.js'></script>
        <title>Libertine Tattoo - Add Item</title>
    </head>
    <body>
        <div id='myModal' class='modal'>
            <!-- Modal content -->
            <div class='modal-content'>
                <div class='modal-header'>
                    <span type='button' class='close' onclick='return closeModal()'>&times;</span>
                    <h2>Libertine Tattoo</h2>
                </div>
                <div class='modal-body'>
                    <p id='modal-text'>The item was successfully added! <br /><br /> Would you like to continue adding new merchandise?</p>
                    <button type='button' class='modal-butt' id='noButt' onclick='cancelClick("manageMerchandise.php")'>No</button>
                    <button type='button' class='modal-butt' id='yesButt' onclick='closeModal()'>Yes</button>
                </div>
            </div>
        </div>

        <nav>
            <ul id='ulNav'>
                <li id='liNav' style='float:left; background-color:#120fbf;'><a id='aNav' href = 'home.php' >Home</a></li>
                <li id='liNav' style='float:left; background-color:#120fbf; border-right:none;'><a id='aNav' href = 'manageWebsite.php' >Main Menu</a></li>
                <li id='liNav'><a id='aNav' href = 'managePortfolio.php' >My Portfolio</a></li>
                <li id='liNav'><a id='aNav' href = 'managePosts.php' >Blog</a></li>
                <li id='liNav'><a id='aNav' href = 'manageMerchandise.php' >Merchandise</a></li>
                <li id='liNav'><a id='aNav' href = 'manageOrders.php' >Orders</a></li>
                <li id='liNav' style='border-right:none;'><a id='aNav' href = 'manageEmployees.php' >Employees</a></li>
                <li id='liNav' style='float:right; background-color:#120fbf;'><a id='aNav' href='logout.php'>Logout</a></li>
            </ul>
        </nav>
        <?php
            /********************************************************************************************************************************************
            *                                              PHP written by: Chris Toth        Date: March 24, 2018                                      *
            *                                             HTML written by: Chris Toth        Date: March 24, 2018                                      *
            ********************************************************************************************************************************************/

            require_once ('checkValidUser.php'); // contains function to validate user (returns bool)
            require_once ('connect.php');        // Contains function to connect to database (returns mysqli connection object)
            require_once "sessionTimer.php";
            session_start();

            $reqAccArr = array("Artist", "Manager");  // array containing required staff positions to access this page

            sessionTimer();

            if(checkValidUser($_SESSION['accessLVL'], $reqAccArr, "")) // checkValidUser returns bool
            {
                // Connect to the database
                $conn = connect();
                
                // Handle the form submission
                if (isset($_POST['save']))
                {
                    //values for the merch_item table
                    $mNAME = $_POST['name'];
                    $mDESC = $_POST['description'];
                    // Clean the price input
                    //
                    // The value is being stored in the database as one integer
                    // move the decimal place
                    // remove the commas
                    $mPRICE = ($_POST['price'] * 100);
                    $mPRICE = intval(str_replace(',', '', $mPRICE));
                    $mPRICE = intval(str_replace('$', '', $mPRICE));
                    $mTYPE = $_POST['type'];
                    $zero = 0;

                    // get values to input into the inventory table
                    $sNONE = $_POST['none'];
                    $sXS = $_POST['xs'];
                    $sS = $_POST['s'];
                    $sM = $_POST['m'];
                    $sL = $_POST['l'];
                    $sXL = $_POST['xl'];
                    $sXXL = $_POST['xxl'];

                    // formats image for database
                    $image = file_get_contents($_FILES['image']['tmp_name']);
                    $extension = strtolower(substr($_FILES['image']['name'], strrpos($_FILES['image']['name'], '.') + 1));
                    $finfo = finfo_open(FILEINFO_MIME_TYPE);
                    $mime = $_FILES['image']['type'];
                    finfo_close($finfo);

        /*****************    Makes sure file is an image of proper type    *****************/
                                                                                            //
                    $extArr = array('png', 'jpg','jpeg','gif');                             //
                    if(!in_array($extension, $extArr))                                      //
                    {                                                                       //
                        echo "<script>"                                                     //
                        ."alert('File Format Must Be png, jpeg/jpg, or gif');"              //
                        ."</script>";                                                       //
                        header("location: addItem.php");                                    //
                        die();                                                              //
                    }                                                                       //
        /************************************************************************************/
                                                                
                    $imageExists = FALSE; 
                    $images = $conn->query("SELECT * FROM IMAGE");

        /***********    Checks if the recieved image already exists in the database    **********/
                    while($row1 = mysqli_fetch_assoc($images))                                  //
                    {                                                                           //
                        if($row1['IMG_DATA'] == $image && $row1['IMG_MIME'] == $mime            //
                        && $row1['IMG_EXT'] == $extension)                                     //
                        {                                                                       //
                            $imageID = $row1['IMG_ID'];                                         //
                            $image = $row1['IMG_DATA'];                                         //
                            $imageExists = TRUE;                                                //
                            break;                                                              //
                        }                                                                       //
                    }

                    if ($imageExists === FALSE)
                    {
                        // If image does not exist. add it.
                        $imgINSERT = $conn->prepare("INSERT INTO IMAGE (IMG_DATA, IMG_MIME, IMG_EXT) VALUES (?, ?, ?);");
                        $imgINSERT->bind_param("bss", $null, $mime, $extension);
                        $imgINSERT->send_long_data(0, $image);
                        $result = $imgINSERT->execute();
                        $imgINSERT->close();

                        if(!$result)
                        {
                            // If query fails output error message and code
                            die($imgINSERT->error ."<br />". $imgINSERT->errno);
                        }
                    }

                    /**************************************    Get ID's to insert into POST_IMAGE    ********************************/
                    $imgID_SQL = $conn->prepare("SELECT IMG_ID FROM IMAGE WHERE IMG_DATA = ? AND IMG_EXT = ?;");                    //
                    $imgID_SQL->bind_param("bs", $null, $extension);                                                                //
                    $imgID_SQL->send_long_data(0, $image);                                                                          //
                    $imgID_SQL->execute();                                                                                          //
                    $imgID_SQL->bind_result($imageID);                                                                              //
                    $imgID_SQL->fetch();                                                                                            //
                    $imgID_SQL->close();


                    // insert into MERCH_ITEM TABLE
                    $sql = $conn->prepare("INSERT INTO MERCH_ITEM (M_NAME, M_DESC, M_TYPE, M_PRICE, M_DELETED) VALUES (?, ?, ?, ?, ?);");
                    $sql->bind_param("sssii", $mNAME, $mDESC, $mTYPE, $mPRICE, $zero);
                    $result = $sql->execute();
                    $sql->fetch();
                    $sql->close();

                    $merchID_SQL = $conn->query("SELECT MAX(M_ID) FROM MERCH_ITEM;");
                    $m_id = mysqli_fetch_assoc($merchID_SQL)['MAX(M_ID)']; 

                    if(!$result)
                    {
                        die($sql->error . $sql->errno);
                    }

                    $merchIMG_SQL = $conn->prepare("INSERT INTO MERCH_IMAGE (IMG_ID, M_ID) VALUES (?, ?);");
                    $merchIMG_SQL->bind_param("ii", $imageID, $m_id);
                    $merch_image_result = $merchIMG_SQL->execute();
                    $merchIMG_SQL->fetch();
                    $merchIMG_SQL->close();

                    if(!$merch_image_result)
                    {
                        die($merch_image_result->error);
                    }

                    // insert into INVENTORY TABLE
                    $sql2 = $conn->prepare("INSERT INTO INVENTORY (M_ID, NA, XS, S, M, L, XL, XXL) VALUES (?, ?, ?, ?, ?, ?, ?, ?);");
                    $sql2->bind_param("iiiiiiii", $m_id, $sNONE, $sXS, $sS, $sM, $sL, $sXL, $sXXL);
                    $result2 = $sql2->execute();
                    $sql2->close();

                    if(!$result2)
                    {
                        die($sql2->error . $sql2->errno);
                    }

                    // prompt success message
                    echo "<script>displayModal();</script>";
                }

                echo "
                <div align ='center' id='wrapper'>
                    <h1>Add Merch</h1>
                    <table>
                        <form action='' method='post' enctype='multipart/form-data'>
                            <tr>
                                <td>
                                    <legend>Item Name</legend>
                                    <input type='text' class='inputField' id='name' name='name' placeholder='Name' value='".(isset($record['M_NAME']) ? $record['M_NAME'] : null)."' required>
                                </td>
                                <td>
                                    <legend>Item Description</legend>
                                    <input type='text' class='inputField' name='description' placeholder='Description' value='".(isset($record['M_DESC']) ? $record['M_DESC'] : null)."' required>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <legend>Price $</legend>
                                    <input type='text' class='inputField' name='price' placeholder='00.00' value='".(isset($record['M_PRICE']) ? number_format(($record['M_PRICE'] / 100), 2) : null)."' required>
                                </td>
                                <td>
                                    <legend>Item Type</legend>
                                    <select name='type' class='inputField' style='width:100%;' required>
                                        <option disabled hidden selected>Select a Type</option>
                                        <option value='Accessories'>Accessories</option>
                                        <option value='Clothing'>Clothing</option>
                                        <option value='Flash Sheets'>Flash Sheets</option>
                                        <option value='other'>Other</option>
                                    </select>
                                </td>
                                </tr>
                            <tr>
                                <td colspan='2'>
                                    <legend>Sizes</legend>
                                    <input type='number' id='none' name='none' value='".(isset($sizes['N/A']) ? $sizes['N/A'] : 0)."' class='size-field' />
                                    <label for='noSize'>No Size</label>
                                    <input type='number' id='XS' name='xs' value='".(isset($sizes['XS']) ? $sizes['XS'] : 0)."' class='size-field' />
                                    <label for='XS'>XS</label>
                                    <input type='number' id='S' name='s' value='".(isset($sizes['S']) ? $sizes['S'] : 0)."' class='size-field' />
                                    <label for='S'>S</label>
                                    <input type='number' id='M' name='m' value='".(isset($sizes['M']) ? $sizes['M'] : 0)."' class='size-field' />
                                    <label for='M'>M</label>
                                    <input type='number' id='L' name='l' value='".(isset($sizes['L']) ? $sizes['L'] : 0)."' class='size-field' />
                                    <label for='L'>L</label>
                                    <input type='number' id='XL' name='xl' value='".(isset($sizes['XL']) ? $sizes['XL'] : 0)."' class='size-field' />
                                    <label for='XL'>XL</label>
                                    <input type='number' id='XXL' name='xxl' value='".(isset($sizes['XXL']) ? $sizes['XXL'] : 0)."' class='size-field' />
                                    <label for='XXL'>XXL</label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan='2'>
                                    <input type='file' name='image' id='image' onchange='changeLbl()' required/>
                                    <label for='image' id='fileUploader'>Add an Image...</label>
                                    <a href='manageMerchandise.php'><input type='button' style='float:right;' class='delbutton' value='Back' /></a>
                                    <input type='submit' name='save' value='Save' formaction='addItem.php' class='button' style='float:right;' />
                                </td>
                            </tr>
                        </form>
                    </table> 
                </div>";
            }
            else
            {
                // User does not have access to this page. Redirect elsewhere
                header("location: invalid.php");
                die();
            }
        ?>
    </body>
</html>