<script>

    function loadBlog(offset)
    {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
            document.getElementById("blogs").innerHTML = this.responseText;
            }
        };
        xhttp.open("GET", "blogPagination.php?offset="+offset, true);
        xhttp.send();

         
       
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0
        
       
    }

</script>
<?php
require_once ("printHTML.php");
require_once "connect.php";
require_once "checkValidUser.php";

session_start();

$conn = connect();

/*$PO_ID = $P_DELETED = '0';
$sql = $conn->prepare("SELECT * FROM POST WHERE PO_ID = ? AND P_DELETED = ?;");
$sql->bind_param("ii", $PO_ID, $P_DELETED);

$result = $sql->execute();*/
//"SELECT * FROM POST WHERE PO_ID = ? AND P_DELETED = ? ORDER BY P_ID DEC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY;";
$limit = 4;
$zero = 0;
//unset($_SESSION['offset']);
if(!isset($_SESSION['offset']))
{
    $_SESSION['offset'] = 0;
}
$offset = $_SESSION['offset'];
$sql = $conn->prepare("SELECT * FROM POST WHERE PO_ID = ? AND P_DELETED = ? ORDER BY P_ID ASC LIMIT ? OFFSET ?;");
if (!$sql)
{
    echo "Error:<br />" . $conn->error ." ".$conn->errno. "<br />";
    $sql->close();
    $conn->close();
    die();
}
$sql->bind_param("iiii", $zero, $zero, $limit, $offset);
$sql->execute();
$result = $sql->get_result();
$sql->fetch();
$sql->close();

if(!isset($_SESSION['max']))
{
    $_SESSION['max'] = $result->num_rows;
}

$max = $_SESSION['max'];

echo "

<!doctype html>

    <head>
        <script src='jquery.min.js'></script>
        <script src='loadingscreen.js'></script>
        <link rel='stylesheet' href='style.css'>
        <link rel='stylesheet' href='loader.css'>
        <link href='https://fonts.googleapis.com/css?family=Work+Sans' rel='stylesheet'>
        <link rel='stylesheet' href='blogV2.css'>
        <title>Libertine Tattoo - Blog</title>
        
       
      
    </head>

    <body>";

    printLoader();    
    printNav();

    echo"   <div id='blogPhoto' class='tile'>
            <div class='center'>
                Blogs
            </div>
    </div>
      ";

        $blogsString = "";
        $iterations = 1;

        while($row = mysqli_fetch_assoc($result))
        {
            $pID = $row['P_ID'];
            $img_sql = $conn->prepare("SELECT * FROM POST_IMAGE, IMAGE WHERE POST_IMAGE.P_ID = ? AND IMAGE.IMG_ID = POST_IMAGE.IMG_ID");
            $img_sql->bind_param("i", $pID);
            $img_sql->execute();
            $imageRes = $img_sql->get_result();
            $img_sql->fetch();
            $img_sql->close();

            $image = mysqli_fetch_assoc($imageRes);
            $imageRes->free();

            //$image = mysqli_fetch_assoc($conn->query("SELECT * FROM POST_IMAGE, IMAGE WHERE POST_IMAGE.P_ID = {$row['P_ID']} AND IMAGE.IMG_ID = POST_IMAGE.IMG_ID"));

            //<h3><p class='pblock{$theNum}'>{$row['P_TITLE']}</p></h3>
            $theNum = ($iterations % 2) + 1;

            $blogsString.="
        <div class='blogtile'>
         
             <div class='imageblock{$theNum}'>
                <image width='100%' height='100%' src = 'data:".$image['IMG_MIME'].";base64,".base64_encode($image['IMG_DATA'])."' />
             </div>

             <div class='textblock{$theNum}'>
                    <h1>{$row['P_TITLE']}</h1>
                  <p>
                   {$row['P_CONTENT']}
                   </p>
                     
                     
              </div>
              
        </div>
        ";
        $iterations++;
        }

        $blogsString.="
      <button class='blogBtn' type='button' name='prev' onclick='return loadBlog(-4)' ".(($offset < 0 || $offset == 0) ? "hidden" : "")."> &#8592; PREV</button>
       <button class='blogBtn' type='button' name='next' onclick='return loadBlog(4)' ".(($offset > $max || $offset == $max) ? "hidden" : "").">NEXT &#8594; </button>
       
      
       ";

        echo "
        <div>
         <div id='blogs'>$blogsString</div>
        </div>
        ";

    
         printFooter();
        echo"
         
        
   
        <button onclick='topFunction()' id='myBtn' title='Go to top'>Top</button>
         
    
        <script>

            window.onscroll = function() {scrollFunction()};

            function scrollFunction() {
                if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                    document.getElementById('myBtn').style.display = 'block';
                } else {
                    document.getElementById('myBtn').style.display = 'none';
                }
            }

            function topFunction() {
                document.body.scrollTop = 0;
                document.documentElement.scrollTop = 0;
            }

        </script>
    </body>

  </html>
";

/*
<link rel='stylesheet' href='blog.css'>

  <style>
        .thisImg
        {
            width:400px;
            height:300px;
            display:inline-block;
            margin:4%;
            position:relative;
            float: left;
        }

        .thisImg img{
            width:100%;
            height:100%;
            border-radius:4px;
            transition:0.5s;
          
        }
        </style>

         <script src='jquery.min.js'></script>
        <script src='faq.js'></script>
            <script src='https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>

          <div id='splashPhoto' class='tile'>
            <img class='center' src='images/blog.png' height='40%' width='35%'></img>
        </div>

         <div id='blogs'>$blogsString</div>
         <div class='blogtile'>$blogsString</div>

         <div class='aboutTeam'>
       <a href='aboutTeam.php' >
       <img src='images/xenial.png' width='20px' height='20px'> 
       </a>
     <div>
*/
?>